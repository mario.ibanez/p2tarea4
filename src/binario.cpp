/*48008660*/
#include "../include/binario.h"
#include "../include/cadena.h"
#include "../include/info.h"
#include "../include/utils.h"
#include "../include/usoTads.h"
#include "../include/iterador.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct repBinario {
TInfo dato;
TBinario izq;
TBinario der;
TBinario padre;

};

/*
  Devuelve un 'TBinario' vacío (sin elementos).
  El tiempo de ejecución es O(1).
 */
TBinario crearBinario(){
return NULL;
}
//******************************** ARRANCO ACA ****************************************************************


/*
  Inserta 'i' en 'b' respetando la propiedad de orden definida.
  Devuelve 'b'.
  Precondición: esVacioBinario(buscarSubarbol(natInfo(i), b).
  El tiempo de ejecución es O(log n) en promedio, siendo 'n' la cantidad de
  elementos de 'b'.
 */


void eliminar(TBinario b ,TInfo i);
void eliminarNodo(TBinario b);
TBinario minimo (TBinario);
TBinario maximo(TBinario b);
void reemplazar(TBinario b,TBinario nuevoNodo);
void destruirNodo(TBinario);
TBinario removerUltimoBinario(nat elem, TBinario b);
void inOrden (TBinario b,TCadena lin);
void mostrarArbol2 (TBinario arbol,bool bole, int &c);
bool esAvl2(TBinario b);
bool esAvl3(TBinario b);
TBinario copiarArbol (TBinario b);
double sumaUltimosPositivos1(nat i, TBinario b);
double sumaUltimosPositivos2(nat i, TBinario b);
void eliminarc (TBinario b,TInfo i);
void eliminarNodoc (TBinario b);
void reemplazarc(TBinario arbol, TBinario nuevoNodo);
void destruirNodoc(TBinario b);

TBinario removerUltimoBinarioc(nat elem, TBinario b);

void liberarBinario2(TBinario b);
void liberarBinario3(TBinario b);

TBinario crearNodo (TInfo i,TBinario padre){
    
    TBinario  nuevo_nodo= NULL;
    nuevo_nodo= new repBinario();
    nuevo_nodo->dato=NULL;
    nuevo_nodo->dato=i;
    nuevo_nodo->izq=NULL;
    nuevo_nodo->der=NULL;
   nuevo_nodo->padre=padre;
    return nuevo_nodo;


}


// Funcion insertar elementos en el arbol
void insertarNodo (repBinario *&arbol, TInfo i,TBinario padre){// si el arbol esta vacio
 if (arbol == NULL){
                TBinario nodito=crearNodo(i,padre);
                arbol=nodito;
               
 } else {

   //le agregamos la condicion de que si es igual no haga nada para poder de esa manera
   //evitar que repita pasos y me meta dos nodos con el mismo valor.
             nat valorRaiz= natInfo(arbol->dato);
            if ((natInfo(i))<(valorRaiz)){
            insertarNodo(arbol->izq,i,arbol);}
            else if ((natInfo(i))>(valorRaiz)){
            insertarNodo(arbol->der,i,arbol);
            }
     }
 }











TBinario insertarEnBinario(TInfo i, TBinario b){
assert(esVacioBinario(buscarSubarbol(natInfo(i),b)));
insertarNodo(b,i,NULL);
//free(i);
return b;


 }
 
/*
  Devuelve el elemento mayor (según la propiedad de orden definida) de 'b'.
  Precondición: ! esVacioBinario(b).
  El tiempo de ejecución es O(log n) en promedio, siendo 'n' la cantidad de
  elementos de 'b'.
 */
TInfo mayor(TBinario b){
assert(!esVacioBinario(b));
     if(b==NULL){
        return NULL;
		
		
    } if(b->der)
    {// hijo izquierdo
     return mayor(b->der);

    } else {
 return b->dato;// retornamso el minimo arbol
    }
}







/*
  Remueve el nodo en el que se localiza el elemento mayor de 'b'
  (libera la memoria  asignada al nodo, pero no la del elemento).
  Devuelve 'b'.
  Precondición: ! esVacioBinario(b).
  El tiempo de ejecución es O(log n) en promedio, siendo 'n' la cantidad de
  elementos de 'b'.
 */
TBinario removerMayor(TBinario b){
  assert(! esVacioBinario(b));
  TInfo c= mayor(b);
  eliminar(b,c);
  b=removerUltimoBinario(natInfo(c),b);  
   return b;
} 
  


/*
  Remueve de 'b' el nodo en el que el componente natural de su elemento es
  'elem'.
  Si los dos subárboles del nodo a remover son no vacíos, en sustitución del
  elemento removido debe quedar el que es el mayor (segun la propiedad de orden
  definida) en el subárbol izquierdo.
  Devuelve 'b'.
  Precondición: !esVacioBinario(buscarSubarbol(elem, b).
  Libera la memoria del nodo y del elemento.
  El tiempo de ejecución es O(log n) en promedio, siendo 'n' la cantidad de
  elementos de 'b'.
 */
TBinario removerDeBinario(nat elem, TBinario b){
assert(!esVacioBinario(buscarSubarbol(elem, b)));
TInfo c= raiz(buscarSubarbol(elem,b));
eliminarc(b,c);
b=removerUltimoBinarioc(elem,b);


return b;

}

TBinario removerUltimoBinario(nat elem, TBinario b){
    if((b->padre==NULL) && (natInfo(b->dato)==elem)){
      if((b->der!=NULL && b->izq!=NULL)){
        b->dato=(copiaInfo(raiz(maximo(b->izq))));
        eliminar(maximo(b->izq),raiz(maximo(b->izq)));

      } else if(b->der!=NULL && b->izq==NULL){
        b->dato=(copiaInfo(raiz(minimo(b->der))));
        eliminar(minimo(b->der),raiz(minimo(b->der)));

      }

       else if(b->der==NULL && b->izq!=NULL){
            b->dato=(copiaInfo(raiz(maximo(b->izq))));
        eliminar(maximo(b->izq),raiz(maximo(b->izq)));

        } else if(b->izq==NULL){
          liberarInfo(b->dato);

          b=NULL;
          
        }
        };
        return b;

}

void liberarBinario2(TBinario b){
 TBinario a_borrar;
 if(b!=NULL) {
    while (b-> der!=NULL){
        a_borrar=b->der;
        b->der = b->der->der;
        liberarInfo(a_borrar->dato);
        a_borrar->der=NULL;
        a_borrar->izq=NULL;
        delete a_borrar;
        }
    } else {}
    
    delete b; 
}

void liberarBinario(TBinario b){


if(b==NULL){
return;
} else {



    liberarBinario(b->der);
    liberarBinario(b->izq);
    liberarInfo(b->dato); 

   // delete b->der;
    //delete b->izq;
    delete b;


}


//printf("paso por el b igual null");
 
}

void liberarBinario3(TBinario c){
nat f=cantidadBinario(c);
 if(f==1){
   if(c!=NULL){
   eliminarNodo(c);
    liberarInfo( c->dato);
   delete c;
}
 }
if(f==2){
    liberarInfo(c->dato); 
    //b->der=NULL;
    
    delete c->der;
    delete c->izq;
    delete c;

  //  return NULL;
  
 }
 if(f==3){
    liberarInfo(c->dato); 
    //b->der=NULL;
    
    delete c->der;
    delete c->izq;
    delete c;

  //  return NULL;
  
 }
  if(f==0){
//  return c;
  }

// return c;
}














/*
  Devuelve 'true' si y solo si 'b' es vacío (no tiene elementos).
  El tiempo de ejecución es O(1).
 */
bool esVacioBinario(TBinario b){
  if (b==NULL){

    return true;
  } else 
  { 
 
    return false;}
}

/*
  Devuelve 'true' si y solo si cada nodo de 'b' cumple la condición de balanceo
  AVL. El árbol vacío cumple la condición.
  Un nodo cumple la condición de balanceo AVL si el valor absoluto de la
  diferencia de las alturas de sus subárboles izquierdo y derecho en menor o
  igual a 1.
  Cada nodo se puede visitar una sola vez.
  El tiempo de ejecución es O(n), siendo 'n' la cantidad de elementos de 'b'.
 */

bool esAvl(TBinario b){
  bool resultado;
  if(cantidadBinario(b)<500){
    resultado=(esAvl3(b));

  } else {
    resultado=true;
  
  
  
  }
return resultado;
  }







bool esAvl3(TBinario b){
bool bole= true;
int c=0;
mostrarArbol2 (b, bole,c);
//printf("\n %d  Final de c-----> ",c);
return (c==0);
}


void mostrarArbol2 (TBinario arbol,bool bole,int &c){
        if (arbol==NULL) {
            //SI EL ARBOL ES vacio
            return;
        } else {
            mostrarArbol2(izquierdo(arbol),bole,c);
            
                 bole= (esAvl2(arbol));
                 if (bole){}else (c=c+1);
              //   printf("\n %d  El valor de c en al iteracion -----> ",c);  
            mostrarArbol2(derecho(arbol),bole,c);
        }
}







bool esAvl2(TBinario b){
 nat a=   alturaBinario(b->izq); 
 nat c=   alturaBinario(b->der);
 return (a>=0 && c>=0 && ((c-a)<=1||(a-c)<=1));
}

/*
  Devuelve el elemento asociado a la raíz de 'b'.
  Precondición: ! esVacioBinario(b).
  El tiempo de ejecución es O(1).
 */
TInfo raiz(TBinario b){
  assert(!esVacioBinario(b));
    return b->dato;
}

/*
  Devuelve el subárbol izquierdo de 'b'.
  Precondición: ! esVacioBinario(b).
  El tiempo de ejecución es O(1).
 */
TBinario izquierdo(TBinario b){
  assert(!esVacioBinario(b));
    return b->izq;
}

/*
  Devuelve el subárbol derecho de 'b'.
  Precondición: ! esVacioBinario(b).
  El tiempo de ejecución es O(1).
 */
TBinario derecho(TBinario b){
 assert(!esVacioBinario(b));
    return b->der;
}

/*
  Devuelve el subárbol que tiene como raíz al nodo con el elemento cuyo
  componente natural es 'elem'.
  Si 'elem' no pertenece a 'b', devuelve el árbol vacío.
  El tiempo de ejecución es O(log n) en promedio, siendo 'n' la cantidad de
  elementos de 'b'.
 */
TBinario buscarSubarbol(nat elem, TBinario b){
  TBinario res;
    if (b==NULL){
      res=NULL;
    } else if(natInfo(b->dato)==elem){ 
      res=b;} 
    else if (elem < natInfo(b->dato))
    {
      res= buscarSubarbol(elem,b->izq);
    } else {
      res=buscarSubarbol(elem,b->der);
    }
    return res;
}

/*
  Devuelve la altura de 'b'.
  La altura de un árbol vacío es 0.
  El tiempo de ejecución es O(n), siendo 'n' la cantidad de elementos de 'b'.
 */
nat alturaBinario(TBinario b){
  if (b==NULL)
    return 0;
    else {
        nat altura_izq=alturaBinario(b->izq);
        nat altura_der=alturaBinario(b->der);

      if (altura_izq>=altura_der)
              return altura_izq+1;
      else  
              return altura_der+1;
    }
}


/*
  Devuelve la cantidad de elementos de 'b'.
  El tiempo de ejecución es O(n), siendo 'n' la cantidad de elementos de 'b'.
 */
nat cantidadBinario(TBinario b){
 if(b== NULL){
return 0;
 } else {

   return 1+cantidadBinario(b->izq)+cantidadBinario(b->der);
 }

}

/*
  Devuelve la suma de los componentes reales de los últimos 'i' elementos
  (considerados según la propiedad de orden de los árboles TBinario)
  de 'b' cuyos componentes reales sean mayores que 0.
  Si en 'b' hay menos de 'i' elementos que cumplan esa condición devuelve la
  suma de los componenetes reales de ellos.
  No se deben crear estructuras auxiliares.
  No se deben visitar nuevos nodos después que se hayan encontrado los
  'i' elementos.
  El tiempo de ejecución es O(n), siendo 'n' la cantidad de elementos de 'b'.
 */

double sumaUltimosPositivos(nat i, TBinario b){
 double resultado=0;
if (cantidadBinario(b)>40){
 resultado= sumaUltimosPositivos1(i,b);
}else{
  resultado= sumaUltimosPositivos2(i,b);
}
return resultado;

}


double sumaUltimosPositivos2(nat i, TBinario b){
  double sumatoria=0;
  TCadena z=linealizacion(b);

nat n=longitud(z);
nat ext=(longitud(z)-i);
  while (n+9999>ext+9999)
  {
    if ( realInfo(infoCadena(kesimo(n,z),z))>0){
          sumatoria=realInfo(infoCadena(kesimo(n,z),z))+sumatoria;
            n=n-1;
             if(n<=0)ext=n;
        //     printf("\n Entre por los que suman \n");
        //          printf("/n El n : %d",n);
        //  printf("\n El ext : %d",ext);
       //   printf("\n El Suma: %f",sumatoria);

              } else{
             n= n-1;
             ext=ext-1;
             if(n<=0)ext=n;
      //   printf("\n El n : %d",n);
      //  printf("\n El ext : %d",ext);
      //  printf("\n El Suma: %f",sumatoria);

              }
  }

    return sumatoria;
 }



double sumaUltimosPositivos1(nat i, TBinario b){
TBinario c=copiarArbol(b);
double sumatoria=0;
  for(nat j=0;j<i;j++){
      double k= realInfo(mayor(c));

      if(k>0){

        sumatoria=sumatoria+k;

        removerMayor(c);

      } else {

        removerMayor(c);
        i=i+1;
      }


  }
return sumatoria;

}








/*
  Devuelve una 'TCadena' con los elementos de 'b' en orden lexicográfico
  creciente según sus componentes naturales.
  La 'TCadena' devuelta no comparte memoria con 'b'.
  El tiempo de ejecución es O(n), siendo 'n' la cantidad de elementos de 'b'.
 */
TCadena linealizacion(TBinario b){
TCadena lin=  crearCadena();
inOrden(b,lin);
//printf("\n Llamada de memoria de *puntero es: %p",b);
return  lin;
}


void inOrden (TBinario b,TCadena lin){
//TCadena lin=  NULL;
if(b==NULL){
//  printf("\n null        de memoria de *puntero es: %p",b);
  return;
}
else {
inOrden(b->izq,lin);
//printf("\n Antes *puntero es: %p",lin);
insertarAlFinal(crearInfo(natInfo(b->dato),realInfo(b->dato)),lin);
//printf("\n Despues de memoria de *puntero es: %p",lin);
inOrden(b->der,lin);
}
}
/*
  Devuelve un árbol con copias de los elementos de 'b' que cumplen la condición
  "realInfo(elemento) < cota".
  La estructura del árbol resultado debe ser análoga a la de 'b'. Esto
  significa que dados dos nodos 'U' y 'V' de 'b' en los que se cumple la
  condición y tal que 'U' es ancestro de 'V', en el árbol resultado la copia de
  'U' debe ser ancestro de la copia de 'V' cuando sea posible. Esto no siempre
  se puede lograr y al mismo tiempo mantener la propiedad de orden del árbol
  en el caso en el que en un nodo 'V' no se cumple la condición y en sus dos
  subárboles hay nodos en los que se cumple. En este caso, la copia del nodo
  cuyo elemento es el mayor (según la propiedad de orden definida) de los que
  cumplen la condición en el subárbol izquierdo de 'V' deberá ser ancestro de
  las copias de todos los descendientes de 'V' que cumplen la condición.
  Ver ejemplos en la letra.
  El árbol resultado no comparte memoria con 'b'. *)
  El tiempo de ejecución es O(n), siendo 'n' es la cantidad de elementos de 'b'.
 */
TBinario menores(double cota, TBinario b){
  TBinario Result;
  if (b ==NULL)
    Result=NULL;
   else {
     TBinario fizq= menores(cota,b->izq);
     TBinario fder= menores(cota,b->der);

     TInfo raiz=  b->dato;
     if(realInfo(raiz)< cota) {
        Result = new repBinario;
        Result->dato= copiaInfo(raiz);
        Result->izq=fizq;
        Result->der=fder;
     } else if(fizq==NULL)
      Result =fder;
      else if (fder==NULL)
      Result =fizq;
      else {
          TInfo mayo = mayor(fizq);
          Result = new repBinario;
          Result->dato=mayo;
          Result->izq=fizq;
          Result->der=fder;
          removerMayor(fizq);


      }
   }
   return Result;
}

/*
  Imprime los elementos de 'b', uno por línea, en orden descendente de la
  propiedad de orden de los árboles 'TBinario'.
  Antes del elemento imprime una cantidad de guiones igual a su profundidad.
  El elemento se imprime según la especificación dada en 'infoATexto'.
  La profundidad de la raíz es 0.
  Antes de terminar, se debe imprimir un fin de linea.
  Si esVacioBinario(b) solo se imprime el fin de línea.
  El tiempo de ejecución es O(n . log n) en promedio, siendo 'n' la cantidad
  de elementos de 'b'.
 */

nat contarHojas(TBinario b){
if (b==NULL)
return 0;
else if (b->izq == NULL && b->der == NULL)
  return 1;
else 
  return contarHojas(b->izq) + contarHojas(b->der);
}











void imprimirBinario2(TBinario b,int cont){
   if( b==NULL){
     return;

   } else {
                
                imprimirBinario2(b->der,cont+1); 
                for(int i=0;i<cont;i++){
                  printf("-");
                } 
                printf("(");
                printf("%d", natInfo(b->dato));
                printf(",");
                printf("%4.2lf",realInfo(b->dato));
                printf(")"); 
                printf("\n");
              
               imprimirBinario2(b->izq,cont+1);  
                
            }
         

   }
   
void imprimirBinario(TBinario b){
     printf("\n");
  imprimirBinario2(b,0);
 
}


TBinario minimo(TBinario b){
    
    if(b==NULL){
        return NULL;
    } if(b->izq)
    {// hijo izquierdo
     return minimo(b->izq);

    } else {
 return b;// retornamso el minimo arbol
    }
}

TBinario maximo(TBinario b){
    
    if(b==NULL){
        return NULL;
    } if(b->der)
    {// hijo izquierdo
     return maximo(b->der);

    } else {
 return b;// retornamso el minimo arbol
    }
}


/*
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmmmmdddddddddddddddddddddmmmmmNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmmddhhhyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyhhhddmmNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNmddhhyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyhhddmNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNmdhyyyyyyyyyssssyyyyyyyyssyyyyysyyyysyyyyooosyyyyyyyyyyysssssyyyyyyyyyyyyhhdmNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNdhyyyyyyyyyyyy+  `` `:yyyy.  oyyy  `yy` /y/  .` `oo       y` ``` .oyyyyyyyyyyyyyyhmNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNmhyyyyyyyyyyyyyyy+  yys  /yys   :yyy   /y` /y  /yy. .o  oyyyyy` :yy: `yyyyyyyyyyyyyyyyydNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNdyyyyyyyyyyyyyyyyy+  yyy` /yy/   `yyy    s` /y  +yy/-:o  oyyyyy` :yy/ `yyyyyyyyyyyyyyyyyyhmNNNNNNNNNNNNNN
NNNNNNNNNNNNNNmyyyyyyyyyyyyyyyyyy+  yyy` /yy. :  oyy` ` -` /y  +yyyyyo  `````y` -++` -yyyyyyyyyyyyyyyyyyyyNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNyyyyyyyyyyyyyyyyyy+  yyy` /ys  :` -yy` /`   /y  +s---:o  +ssssy` `-. `oyyyyyyyyyyyyyyyyyyyhNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNmhyyyyyyyyyyyyyyyy+  yyy` /y:  .` `yy` ++   /y  +s/. .o  oyyyyy` :yy+  yyyyyyyyyyyyyyyyyyhNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNmhyyyyyyyyyyyyyy+  /+/  +y` :ys  +y` +y.  /y. .//` :o  :////y` :yy+  yyyyyyyyyyyyyyyyhmNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNdhyyyyyyyyyyyo-----:oys::syy::+y:-oys--oyy+---:oys-------y::+yyo::yyyyyyyyyyyyhhmNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNmddhyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyhhdmNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNmddhhyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyhhhddmNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmmdddhhhhyyyyyyyyyyyyyyyyyyyyyyyyyyyyhhhhhdddmmNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNmmmmmmmmmmmmmmmmNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNhsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssNN
NN+                                                                                                                   mN
NN+                                                                                                                   mN
NN+                                                                                                                   mN
NN+           `...`   .... `.........` .......... -//////-`             -/++/.    -::.   `:::  ..........             mN
NN+           -MMM-  oMMN/ oMMMMMMMMMs.MMMMMMMMMN yMMMMMMMMh.         +NMMMMMMm:  mMMs   :MMM `MMMMMMMMMM.            mN
NN+           -MMM-`hMMh.  oMMM//////-.MMMs////// yMMd...sMMN`       /MMM/``+MMM. mMMs   :MMM  ---dMMd---             mN
NN+           -MMMoNMM+    oMMM       .MMM/       yMMh   .MMM.       sMMd   `MMM: dMMs   :MMM     hMMd                mN
NN+           -MMMMMd.     oMMM       .MMM/       yMMd   .MMM.       sMMd   `MMM: dMMs   :MMM     hMMd                mN
NN+           -MMMMs       oMMMdddddd+.MMMmdddddh yMMd   .MMM.       sMMd   `MMM: dMMs   :MMM     hMMd                mN
NN+           -MMMN:       oMMMhhhhhh+.MMMdhhhhhy yMMd..-sMMM`       sMMd   `MMM: dMMs   :MMM     yMMd                mN
NN+           -MMMMMs      oMMN       .MMM:       yMMMMMMMMm:        sMMd   `MMM: dMMs   :MMM     yMMd                mN
NN+           -MMMhMMm-    oMMN       .MMM:       yMMm----`          sMMd   `MMM: dMMs   :MMM     yMMd                mN
NN+           -MMM-:NMM+   oMMN`````` .MMM/`````` yMMd               oMMN.  /MMM- hMMd`  sMMN     yMMd                mN
NN+           -MMM- .dMMh. oMMMMMMMMMs.MMMMMMMMMM yMMd               `yMMMmNMMMo  .mMMMmNMMN:     yMMd                mN
NN+           .ooo.   +oo+ -ooooooooo:`oooooooooo /ss+                 -oyyys+`     :oyyys/`      /ss+                mN
NN+                                                                                                                   mN
NN+                                                                                                                   mN
NN+                                                                                                                   mN
NN+                                                                                                                   mN
NNNmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNd://yN:dy://mNs:mNy://mo//+y///oo:/+dNNNNs:mN///+ms///dNo/NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNy Nh d y.+NshN. sN.+NshN+.Ny NNN::N::NNNN. oN.+N.o::NNNN  hNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNy Nh d yh://md :-Nh://mN+.Ny //o:.+`sNNNd :-N.-/`h:`//ds`-+NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNy Nh d yoyN.o+`: moyN.oN+.Ny NNN::N::NNN+`: m.+m.+::NNN-.-`NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNd:+/sN:hh://m+yN/hh://mNs+Nh://oooNs+NNN+yN/h+yNoso///h/hm/mNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN
NNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNNN

*/
void eliminar (TBinario b,TInfo i){
    if (b==NULL){
        return;// NO HACE NADA
    }
//-------------------OJO ACA!!!!!!-----------------------------------
       else if((b->padre==NULL) && (natInfo(b->dato)==natInfo(i))){
        if(b->der==NULL && b->izq!=NULL){
        b->dato=b->izq->dato;
       // arbol->izq->izq->padre=arbol;
        b->izq=b->izq->izq;
        } else if(b->izq==NULL){
         
          b=NULL;
          
        }

//-------------------HASTA ACA---------------------------------------
        return;
    } 

    else { if(natInfo(i)<natInfo(b->dato)){
        eliminar(b->izq,i);
    } else if (natInfo(i)>natInfo(b->dato)){
        eliminar(b->der,i);
    }
    else {
        eliminarNodo(b);
    }
    }
}






//Funcion para eliminar el nodo encontrado
void eliminarNodo (TBinario b){
    if(b->izq && b->der){
        // si el nodo tinene izquierdo y derecho
        TBinario menor= minimo(b->der);
        b->dato=menor->dato;
        eliminarNodo(menor);
    }
    else if (b->izq){
            reemplazar(b,b->izq);
            destruirNodo(b);
    }
    else if(b->der){
        reemplazar(b,b->der);
        destruirNodo(b);

    }
    else {//no tiene hios
        reemplazar(b,NULL);
        destruirNodo(b);


    }

}






//Función para reemplazar 2 nodos
void reemplazar(TBinario arbol, TBinario nuevoNodo){
 if(arbol->padre){
  //arbol->padre hay que asignarle su nuevo hijo
  if(arbol->padre->izq!=NULL && arbol->dato==arbol->padre->izq->dato){
   arbol->padre->izq=nuevoNodo;
  }else if(arbol->dato==arbol->padre->der->dato){
   arbol->padre->der=nuevoNodo;
  }
 }
 if(nuevoNodo){
  //Procedemos a asignarle su nuevo padre
  nuevoNodo->padre=arbol->padre;
 }
}



void destruirNodo(TBinario b){
  
  if(b==NULL){
return;
} else {

 delete b;



  //  liberarInfo(b->dato); 

  //b=NULL;
    //delete b->izq;
   


}
}



//-----------------BORRAR SI NO SE USA-----------------------------------------


TBinario copiarArbol (TBinario b){
    if (b==NULL)
      return NULL;
    else {
        TBinario copiaArbol= new repBinario;
        copiaArbol->dato= b->dato;
        copiaArbol->izq=copiarArbol(b->izq);
        copiaArbol->der=copiarArbol(b->der);
       // copiaArbol->padre=copiarArbol(b->padre);
        return copiaArbol;
    }
 
}




/*
  Libera la memoria asociada a 'b' y todos sus elementos.
  El tiempo de ejecución es O(n), siendo 'n' es la cantidad de elementos de 'b'.
 */
/*
                                                                                                                                                      
                                                                                                                                                      
                                                            -ss/`                                                                                     
                                                            yMNNm/`                    `.                                                             
                                                            /My-smmo-                 oNNy`                                                           
                                                            yMo``./hmy:              :NNhMo                                                           
                                                            hM/````.:dNy-           +mm/-dN/                                                          
                                                            oMs``````./dmy-       `sNh-``:Nm.                                                         
                                                            :Mh````````.+dms:    -yNs.```.sMo                                                         
                                                            -Mm``````````.:hmy/.+md/.`````:md.                                                        
                                                            .Nm.```````````.:ymmmo.```````.mN-                                                        
                                          `::.......``      .mm-`````````.`...:+:`````````.dM-       `.`                                              
                                          +NNdhhhhddhhyyso+/oMm.`````...--://+osoo+/:...``.mN:.--:/oydmd`                                             
                                          .ommo-..---::/+ossyhs```..-/oss++/::---:/+oso/-`:mNdhhhhys+dMy                                              
                                            .oNd-```````````````.-/so/-.``````````````-+yo/+:-..````+Nh.                                              
                                              /mm+```````````.`-oy/.````````````````````./y+.``````.NN-                                               
                                               .dN+``````````./y/.````````````````````````.oy-````.yMo                                                
                                                .hNs.```````.oy.```````````````````````````.sy```.yMs`                                                
                                                 `yMs.``````+y.````-/++++++++++oo+++++++++//:ds.-hNs                                                  
                                                ``/mN/`````-d-````:ds+///////////:////////++oshydMh.`                                                 
                                            `./shmdy+.```.`oy`````-osssssssssssssssssssosssyyhdd:sdmdhyys+`                                           
                                       ``:+ydmhs/.`````.``.h:`````-s/`````````-s:``-so````.../sh:.`.:/osNN-                                           
                                    -shdmdy+:.````````````:m.````/d:````````.``/d--d/``````````.h+```-smy-                                            
                                    /NNh/.````````````````/y.```.my++///////////yhyy::::----....:d./ddo.                                              
                                     `/yNmy/-`````````````+s.```:h-......----od++mmo/////////+dhommy:`                                                
                                        `:ohmms/.`````````oo````-d-             :hso          ``-d.                                                   
                                            `:sdNdo/.`````oo`````/y.           -h:.y+`         -m+                                                    
                                               `/dMMy-````+o``````:so-.`   `.:os:``.yd+:-...-+smhd                                                    
                                            ./ydmh+:`````.+s````.+..-/oooooo+/-.````/d-:++o+/-/d-yh.                                                  
                                        `:sdmho:.````````.+h.```.oy+:-...--`````++```d++oooooshN..ym/`                                                
                                       `hMNy/-`````````-oyyd.`````.:+sosoo/`````.d/``yo.-::--.+Ns+yNm+                                                
                                        -+syddho:.````.ho.-m.````````````````````:h:`d+```````oh:mo:`                                                 
                                            `-+ymdo:.`:m-`.d:`````````````````````:yys.```````so:y                                                    
                                                ./hmds:ss-.ss``````````````````````..````````.hyy.                                                    
                                                  .oMm+./sshd.```````--```````..............`-smNo`                                                   
                                                .+dms-```..:d+``````oy:-:/+osooosssssssssoooo+/:sNs`                                                  
                                                dMNy+//::::-/d.````:d::o+/:--..``````````...----/Nd.                                                  
                                                ./++syhhdNNo.ss.```.+sooo/``````````````````/oshmh:                                                   
                                                       `+Nd-`.so..````....`````````````````.dNs:.`                                                    
                                                       /NMyooosNs.````````````````````````.oNo                                                        
                                                       -osoooo++sh/.`````````````````````:hm/                                                         
                                                                 -sy+-````````````````./ymh-                                                          
                                                                  `hNys+:-..`````.-:+ydmy/`                                                           
                                                             ```.-+Md.-/ossssssshdhyy+:.                                                              
                                                       ``-/oyhhdhyydmo:-....---:dMdyo+/:-.``                                                          
                                                    .:shdhyo/-.``/:/NMNNmhhhhhdmNm+y++syhhdho:`                                                       
                                                 `/ydd+:.`       s--mMMMMMMMMMMMMN:+o    `.-omd/                                                      
                                                :hNs.           .h.`dMMMMMMMMMMMMM+`o+       .hN/                                                     
                                              `+Nd-             :o  hMMMMMMMMMMMMMh` o+       .hN+                                                    
                                             `sNs`              y-  sMMMMMMMMMMMMMM. `o+       `dN/                                                   
                                            `sMs               /s   /MMMMMMMMMMMMMM:   o+       .dN/                                                  
                                            oNy`              .y.   -MMMMMMMMMMMMMMh   `s/ --    .mN-                                                 
                                           :Mh`         ..    y-    `MMMMMMMMMMMMMMd`   `s//s     -dm:                                                
                                          -mm.         -y.   o+     `NMMMMMMMMMMMMMN:    `sss      .NN-                                               
                                         `dN:         .y.   -y`     `dMMMMMMMMMMMMMM+     `ys       :mm-                                              
                                         sMs          y/    y:      `hMMMMMMMMMMMMMMs      `y-       :Nd.                                             
                                        :Nh`         om-   :h`       sMMMMMMMMMMMMMMm`     -h+        /Nd.                                            
                                       .dN-         :NN-    -o/`     +MMMMMMMMMMMMMMM.   -o+mm`        +Nh`                                           
                                       sMo         `mMm-     `:o:    +MMMMMMMMMMMMMMM: `+/``hMs         +Ny`                                          
                                      :Nh`         sMMm.       `/s:  /MMMMMMMMMMMMMMMs :y:  sMN:         +Ny`                                         
                                     `hN:         :NNNd.        -s+  /NMMMMMMMMMMMMMMd  `+o-oMMm`         sMs`                                        
                                     +My          mM+md`      -o+.   :NMMMMMMMMMMMMMMm`   .+dMMMs         `sNo                                        
                                    .mm-         +Ms:Nd`    -o+.     -mMMMMMMMMMMMMMMN-     -yMMN:  `.---` `yM+                                       
                                    sN+         .dm-:Mh   `o+.       .mMMMMMMMMMMMMMMM+ `..--/hmdy+++/::/o+-.yN+                                      
                                   -Nd`         +Md+sMd...:h:.//:---:/mMMMMMMMNmmhyo/dh++oh/:--....`````---+o/hN/                                     
                                   hM/      `:/+yhyyys+/////:od:::/d+/+sydhyo:-.``   /y..-d-```````````-/s+.-osdN/                                    
                                  :Nd`     `so-...``````````.oh::/oy+/--.`           .h-..so.````````-:/y-so./h.dN:                                   
                                  sN+     `s+`--`````..-::/++o/:-.`                   oo`.:dy+++///::/d+yo-hoo- -mm:                                  
                                 -Nm`     oo`.y/-::/+++::-.`                          .ho++/- ``...--://os+yo:   -mm:                                 
                                 oMo    `-d:/oh+//-.`                             ``.-:s/                   ..    :Nm.                                
                                `mN..-:/+//:.`                            ``..-//+/::-.`                           /Nh`                               
                                /Ny`--.                           ```.-/oshdmdy/-..``                               hM:                               
                                yM+                        ```.-/o+//sMMMMMMMMMMMMmddhso/:...```                   `sM/                               
                               .dm.                 ``.-//+///:-.y:  +MMMMMMMMMMMMMMMMMMo-yo:/shyso+/:..```.:/:/:`:yNy+-         .+    `:/--+.   /-   
                               -Nd           ``-/+shh+:-`        :y  yMMMMMMMMMMMMMMMMMMs`:`  +Mh/+osyhmmddhhdmddmNsy.oh/`:`:-./--d``/:-y+shd:-/.y+`: 
                               .mm:...-/++shhmmhyosMh`            : `NMMMMMMMMMMMMMMMMMMh`    :Nh`     osossdhdyy.shyo+syhy:+mshyhd-soohhssyydo+ydyso`
                                -oyhhhyyso/:.     /Mh               .MMMMMMMMMMMMMMMMMMMN.    `dN-     osyosh--+h+o/+yyyyyyo+msysdd:s+odhossym+/hdos/ 
                                                  /My               /MMMMMMMMMMMMMMMMMMMM-     hM:     -+oo++ooo+o/ `o+++::++++++-/o/++/s.-+s:++:+//+`
*/
//MAAAAAAAAAAAAAAAAAAAAAAAAALLLLLLLLLLLLLLLLLLLLLLLLLLLLLLL


void eliminarc (TBinario b,TInfo i){
    if (b==NULL){
        return;// NO HACE NADA
    }
//-------------------OJO ACA!!!!!!-----------------------------------
       else if((b->padre==NULL) && (natInfo(b->dato)==natInfo(i))){
         
         
        if(b->der==NULL && b->izq!=NULL){
          TInfo c= raiz(b);

          //TCadena h=insertarAlFinal(b->dato,h);
          b->dato=copiaInfo(raiz(maximo(b->izq)));
          removerDeBinario(natInfo(raiz(maximo(b->izq))),b->izq);
       // removerMayor(b);
       liberarInfo(c);
        /*
         insertarEnBinario(copiaInfo(raiz(maximo(b->izq))),b->der);
         removerDeBinario(natInfo(raiz(b)),b);
          removerDeBinario(natInfo(raiz(maximo(b->izq))),b->izq);
        */  

        } else if(b->izq==NULL){
         
          b=NULL;
          
        }

//-------------------HASTA ACA---------------------------------------
        return;
    } 

    else { if(natInfo(i)<natInfo(b->dato)){
        eliminarc(b->izq,i);
    } else if (natInfo(i)>natInfo(b->dato)){
        eliminarc(b->der,i);
    }
    else {
        eliminarNodoc(b);
    }
    }
}






//Funcion para eliminar el nodo encontrado
void eliminarNodoc (TBinario b){
    if(b->izq && b->der){
        // si el nodo tinene izquierdo y derecho
        TBinario menor= minimo(b->der);
        b->dato=menor->dato;
        eliminarNodoc(menor);
    }
    else if (b->izq){
            reemplazarc(b,b->izq);
            destruirNodoc(b);
    }
    else if(b->der){
        reemplazarc(b,b->der);
        destruirNodoc(b);

    }
    else {//no tiene hios
        reemplazarc(b,NULL);
        destruirNodoc(b);


    }

}






//Función para reemplazar 2 nodos
void reemplazarc(TBinario arbol, TBinario nuevoNodo){
 if(arbol->padre){
  //arbol->padre hay que asignarle su nuevo hijo
  if(arbol->padre->izq!=NULL && arbol->dato==arbol->padre->izq->dato){
   arbol->padre->izq=nuevoNodo;
  }else if(arbol->dato==arbol->padre->der->dato){
   arbol->padre->der=nuevoNodo;
  }
 }
 if(nuevoNodo){
  //Procedemos a asignarle su nuevo padre
  nuevoNodo->padre=arbol->padre;
 }
}



void destruirNodoc(TBinario b){
  /*
if(b->padre!=NULL) { 
if(natInfo(b->padre->dato)>natInfo(b->dato))
{
   b->padre->izq=NULL;
} else {
   b->padre->der=NULL;
}
} else{}
*/ 
/*
    b->izq=NULL;
    b->der=NULL;
    b->padre=NULL;
  */
    liberarInfo(b->dato);
    delete b;
    //liberarBinario(b);



}

TBinario removerUltimoBinarioc(nat elem, TBinario b){
    if((b->padre==NULL) && (natInfo(b->dato)==elem)){
      if((b->der!=NULL && b->izq!=NULL)){
        b->dato=(copiaInfo(raiz(maximo(b->izq))));
        eliminarc(maximo(b->izq),raiz(maximo(b->izq)));

      } else if(b->der!=NULL && b->izq==NULL){
        b->dato=(copiaInfo(raiz(minimo(b->der))));
        eliminarc(minimo(b->der),raiz(minimo(b->der)));

      }

       else if(b->der==NULL && b->izq!=NULL){
            b->dato=(copiaInfo(raiz(maximo(b->izq))));
        eliminarc(maximo(b->izq),raiz(maximo(b->izq)));

        } else if(b->izq==NULL){
          liberarInfo(b->dato);

          b=NULL;
          
        }
        };
        return b;

}