/*48008660*/
#include "../include/cadena.h"
#include "../include/info.h"
#include "../include/utils.h"
#include "../include/usoTads.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>

struct nodo {
    TInfo dato;
    TLocalizador anterior;
    TLocalizador siguiente;
};

struct repCadena {
TLocalizador inicio;
TLocalizador final;
};

bool esLocalizador (TLocalizador loc) { return loc != NULL; } // entendi, retorna true si es distinto de null como se pedia ok

TCadena crearCadena() {
  // printf("creee cadenaaa \n");
    TCadena res = new repCadena; // crea puntero
    res->inicio = res->final = NULL; // hace los apuntes correspondientes
//  printf("creee cadenaaa \n");
    return res; // lo devuelve
}

/*
  Libera la memoria asignada a 'cad' y la de todos sus elementos.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos en 'cad'.
 */




void liberarCadena (TCadena cad){
    TLocalizador a_borrar;
    while (cad-> inicio!=NULL){
        a_borrar=cad->inicio;
        cad->inicio = cad->inicio->siguiente;
        liberarInfo(a_borrar->dato);
        delete a_borrar;
        }
    delete cad; 
}


bool esVaciaCadena(TCadena cad){
    assert(((cad->inicio==NULL)&&(cad->final == NULL)) ||
    ((cad->inicio != NULL)&& (cad->final !=NULL)) );
    return ((cad->inicio==NULL)&&(cad->final== NULL));
}

TLocalizador inicioCadena (TCadena cad){
    TLocalizador res;
    if (esVaciaCadena(cad)){
        res = NULL;
   } else {
       res = cad->inicio;
   }   
   return res;
}


TLocalizador siguiente (TLocalizador loc,TCadena cad){
assert (localizadorEnCadena(loc,cad));
TLocalizador res;
    if (esFinalCadena(loc,cad)){
        res = NULL;
    } else {
        res= loc->siguiente;
    }
return res;

}

bool localizadorEnCadena (TLocalizador loc, TCadena cad){
TLocalizador cursor = inicioCadena(cad);
while (esLocalizador(cursor) && (cursor !=loc))
cursor = siguiente(cursor,cad);
return esLocalizador(cursor);
}

/*
  Devuelve 'true' si y solo si 'loc1' es igual o precede a 'loc2' en 'cad'.
  Si esVaciaCadena (cad) devuelve 'false'.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos en 'cad'.
 */

bool precedeEnCadena(TLocalizador loc1,TLocalizador loc2, TCadena cad){
 bool res;
    res =localizadorEnCadena (loc1,cad);
    if (res) {
            TLocalizador cursor= loc1;
            while (esLocalizador(cursor) && (cursor != loc2))
            cursor = siguiente(cursor,cad);
            res = esLocalizador (cursor);
            assert(!res || localizadorEnCadena(loc2,cad));
    }
    return res;
}
/***************************************HASTA ACA LO QUE ELLOS NOS DIERON*********************************************

*/
TLocalizador finalCadena(TCadena cad){
  
if ( esVaciaCadena(cad))
    {
    return NULL;
    } else {
   return cad->final;
    }
}



TInfo infoCadena(TLocalizador loc, TCadena cad){
assert(localizadorEnCadena(loc,cad));
return loc->dato;   
}



TLocalizador anterior(TLocalizador loc, TCadena cad){
  if (esInicioCadena(loc, cad)){
     return NULL;
  }
    return loc->anterior;
}


bool esFinalCadena(TLocalizador loc, TCadena cad){
  //printf("La dirección de memoria de *puntero es: %p",cad);
  //printf("La dirección de memoria de *puntero es: %p",cad->final);
  // printf("La dirección de memoria de *puntero es: %p",loc->siguiente);
  //  printf("La dirección de memoria de *puntero es: %p",loc);
if (cad->final == NULL){
  return false;
}
   return ((cad->final == loc)) ;
}


bool esInicioCadena(TLocalizador loc, TCadena cad){
  if (esVaciaCadena (cad)){
    return false;
  } else { return (cad->inicio==loc);}
}

/*
  Se inserta 'i' como último elemento de 'cad'.
  Devuelve 'cad'.
  Si esVaciaVadena (cad) 'i' se inserta como único elemento de 'cad'.
  El tiempo de ejecución en el peor caso es O(1).
 */

TCadena insertarAlFinal(TInfo i, TCadena cad){
//TCadena nodos=crearCadena();

TLocalizador nodito = new nodo;


nodito->dato=i;



                
  

  if(esVaciaCadena(cad)){
               // printf("paso por es vacia");
                //nodito ->dato = copiaInfo(i);
                nodito ->anterior = NULL;
                nodito ->siguiente = NULL;
                cad->final= nodito;
                cad->inicio= nodito;
          
    } else{
  // printf("paso por es paso por incio");
    //nodito ->dato =copiaInfo(i);
    cad->final->siguiente= nodito;
    nodito ->anterior = cad->final;
    nodito ->siguiente = NULL;   
    cad->final= nodito;
}

/*
printf("\n %p  agregado NODITO-----> ",nodito->dato);
printf("\n %p  SALGO NODITO -----> ",nodito);
printf("\n %p  SALGO NODITO DATO-----> ",nodito->dato);
printf("\n %p  SALGO NODITO ANTERIOR----> ",nodito->anterior);
printf("\n %p  SALGO NODITO SIGUIENTE-----> ",nodito->siguiente);
*/

  
  //liberarInfo(i);
  //liberarCadena(nodos);
  return cad;
    
}

TCadena insertarAntes(TInfo i, TLocalizador loc, TCadena cad){
//  printf("%d\n",localizadorEnCadena(loc, cad));//  me dio true
  assert(localizadorEnCadena(loc, cad));
 // printf("%d\n",localizadorEnCadena(loc, cad));//  me dio true
  TLocalizador nodito = new nodo;
  TInfo nueva = copiaInfo(i);
nodito->dato=NULL;
/*
printf("\n %p  SALGO NODITO DATO-----> ",nodito->dato);
printf("\n %p  SALGO NODITO -----> ",nodito);
printf("\n %p  SALGO NODITO DATO-----> ",nodito->dato);
printf("\n %p  SALGO NODITO ANTERIOR----> ",nodito->anterior);
printf("\n %p  SALGO NODITO SIGUIENTE-----> ",nodito->siguiente);
*/
nodito->siguiente=NULL;
nodito->anterior=NULL;



   if(esVaciaCadena(cad)){
  //    printf("paso por es vacia");
    nodito ->dato = nueva;
    nodito ->anterior = NULL;
    nodito ->siguiente = NULL;
    cad->final= nodito;
    cad->inicio= nodito;
  
    } else if(loc->anterior==NULL){
     // printf("paso por es paso por incio");
             nodito ->dato = nueva;
             nodito ->anterior = NULL;
             nodito ->siguiente = loc;
             loc->anterior= nodito;
             cad->inicio= nodito;
         
    }
           else {
         //    printf("paso por es final");
             nodito ->dato = nueva;
             nodito ->anterior = loc->anterior;
             nodito ->siguiente = loc;
             loc->anterior->siguiente = nodito;
             loc->anterior= nodito;
            
}
nodito= NULL;
    liberarInfo(i);
    return cad;
}
  
/*
  Se remueve el elemento al que se accede desde 'loc' y se libera la memoria
  asignada al mismo y al nodo apuntado por el localizador.
  Devuelve 'cad'.
  El valor de 'loc' queda indeterminado.
  Precondición: localizadorEnCadena(loc, cad).
  El tiempo de ejecución en el peor caso es O(1).
 */


































TCadena removerDeCadena(TLocalizador loc, TCadena cad){
  assert(localizadorEnCadena(loc, cad));
   if(!esInicioCadena(loc, cad) && !esFinalCadena(loc,cad) && !esVaciaCadena(cad)){
    loc->siguiente->anterior= loc->anterior;
    loc->anterior->siguiente= loc->siguiente;
  
  
   } else if (esInicioCadena(loc, cad)){
              if(loc->siguiente==NULL) {
                  cad->inicio=NULL;
                  cad->final=NULL;
                  //delete loc;


              } else {
             loc->siguiente->anterior= NULL;
             cad->inicio=loc->siguiente;
                      }

   } else
   {
     loc->anterior->siguiente= NULL;
     cad->final=loc->anterior;

   }

    liberarInfo(loc->dato);
    loc->anterior=NULL;
    loc->siguiente=NULL;
    loc->dato=NULL;
     delete loc;
     return cad;
}





/*
  Imprime los elementos de 'cad' de la siguiente forma:
  (n de pos1,r de pos1)(n de pos2,r de pos2) ...
  donce 'n` es el componente natural y 'r' es el componente real.
  Antes de terminar, se debe imprimir un fin de linea.
  Si esVaciaCadena(cad) sólo se imprime el fin de línea.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos en 'cad'.
 */





void imprimirCadena(TCadena cad){

          TLocalizador loc =cad->inicio;
          nat numerador = longitud(cad);
            
            while(numerador>0 && loc!=NULL){
                printf("(");
               printf("%d", natInfo(loc->dato));
                printf(",");
                printf("%4.2lf",realInfo(loc->dato));
                printf(")");
      //ArregloChars txtInfo = infoATexto(infoCadena(loc, cad));
                
                loc=siguiente(loc,cad);
                numerador=numerador-1;
                
            }
          printf("\n");
}












TLocalizador kesimo(nat k, TCadena cad){
  TCadena cad2=cad;
  cad2->final=cad->final;
  cad2->inicio=cad->inicio;
  TLocalizador loc2 =cad2->inicio;
 
      for(int numerador = k;(numerador)>1 && loc2!=NULL;numerador--){
         loc2=siguiente(loc2,cad2);
      }
 
      if ((loc2!=NULL && k>0)){
        loc2=loc2;
       // printf("entro al true---------->");
       //    printf("La dirección de memoria de *puntero es: %p",loc2);
      //    printf("entro al true");
        return loc2;
    } else { 
      //   printf("entro al false--------->");  
     //  printf("La dirección de memoria de *puntero es: %p",loc2);
           return NULL;
    }
    
}


/*
  Se inserta en 'cad' la 'TCadena' 'sgm' inmediatamente después de 'loc',
  manteniendo los elementos originales y el orden relativo entre ellos.
  Devuelve 'cad'.
  No se debe obtener memoria (los nodos de 'sgm' pasan a ser parte de 'cad').
  Se libera la memoria asignada al resto de las estructuras de 'sgm'.
  El valor de 'sgm' queda indeterminado.
  Si esVaciaCadena(cad) 'loc' es ignorado y el segmento queda insertado.
  Precondición: esVaciaCadena(cad) o localizadorEnCadena(loc, cad).
  El tiempo de ejecución en el peor caso es O(1).
 */


TCadena insertarSegmentoDespues(TCadena sgm, TLocalizador loc, TCadena cad){
  assert(esVaciaCadena(cad) || localizadorEnCadena(loc, cad));

  if(esVaciaCadena(cad))
  { 
     cad->inicio=sgm->inicio;
 
     cad->final= sgm->final;} 
          else if (!esFinalCadena(loc,cad) && !esVaciaCadena(sgm))
          {
           sgm->final->siguiente=loc->siguiente;
           sgm->inicio->anterior=loc;
            loc->siguiente->anterior=sgm->final;
            loc->siguiente=sgm->inicio;
            

          } else if (esFinalCadena(loc,cad) && !esVaciaCadena(sgm) )
          {      
                   cad->final->siguiente=sgm->inicio;
                   sgm->inicio->anterior=cad->final;                
                   cad->final=sgm->final;
  
          }
    sgm->final=NULL;
    sgm->inicio=NULL;
    liberarCadena(sgm);
 
    return cad;
}


/*
  Devuelve una 'TCadena' con los elementos de 'cad' que se encuentran entre
  'desde' y 'hasta', incluidos.
  La 'TCadena' resultado no comparte memoria con 'cad'.
  Si esVaciaCadena(cad) devuelve la 'TCadena' vacia.
  Precondición: esVaciaCadena(cad) o precedeEnCadena(desde, hasta, cad).
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos en la cadena resultado.
*/



TCadena copiarSegmento(TLocalizador desde, TLocalizador hasta, TCadena cad)
{
 assert(esVaciaCadena(cad) ||  precedeEnCadena(desde, hasta, cad) );
 
  
  TCadena cad2 =crearCadena();
  cad2 ->inicio = cad2->final = NULL;  
  TInfo insumo;
  
  TLocalizador numer= desde;

 


  if(!esVaciaCadena(cad) && precedeEnCadena(desde, hasta, cad) && desde!=NULL)
  {

    for(int numerador = longitud(cad)+1;numer!=NULL&& numer->dato!=NULL &&numer!=hasta->siguiente &&(numerador)>1 ;numerador--)
    {
        insumo= copiaInfo(numer->dato);
        numer=siguiente(numer,cad);
      
        insertarAlFinal(insumo,cad2);

      }
    
  }

   
return cad2;
//FinimprimirCadena(cad2);
    
}


/*
  Remueve de 'cad' los elementos que se encuentran  entre 'desde' y 'hasta',
  incluidos y libera la memoria que tenían asignada y la de sus nodos.
  Devuelve 'cad'.
  Si esVaciaCadena(cad) devuelve la 'TCadena' vacía.
  Precondición: esVaciaCadena(cad) o precedeEnCadena(desde, hasta, cad).
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos en la cadena resultado.
 */


TCadena borrarSegmento(TLocalizador desde, TLocalizador hasta, TCadena cad)
{  assert( esVaciaCadena(cad) ||  precedeEnCadena(desde, hasta, cad));

    if(!esVaciaCadena(cad) && desde!=0 && hasta!=0)
      {  
                  // TCadena cad2 =copiarSegmento(desde,hasta,cad);
                   
                   // printf("entro al if--------->");  
                        for(int numerador = 99; hasta!=0 && !esVaciaCadena(cad) && desde!=hasta && desde->siguiente!=hasta &&(numerador)>1 ;numerador--)
                        {
                        //   printf("\n entro al For-->"); 
                           
                             
                        //  printf(" \n entro al for--------->\n");  
                          removerDeCadena(desde->siguiente,cad);

                         
                          
                      //    printf(" \n sali del for---------> \n"); 
                          //imprimirCadena(cad); 
                        }
                         
                          if(desde==hasta && !esVaciaCadena(cad) )
                          {
                       //       printf(" \n Dentro del if uno---------> \n");
                               removerDeCadena(hasta,cad); 
                               
                          } else if ((desde->siguiente=hasta) && !esVaciaCadena(cad))
                          
                          {
                         //      printf(" \n ULTIMO IF--------> \n");
                              removerDeCadena(desde->siguiente,cad);
                              removerDeCadena(desde,cad);

                           } 
                          

  else { return cad;

                           }
      }  return cad; 
}
       
               

/*
  Sustituye con 'i' el elemento de 'cad' al que se accede con 'loc'.
  Devuelve 'cad'.
  No destruye el elemento al que antes se accedía con 'loc'.
  Precondición: localizadorEnCadena(loc, cad).
  El tiempo de ejecución en el peor caso es O(1).
 */


TCadena cambiarEnCadena(TInfo i, TLocalizador loc, TCadena cad)
{
  assert(localizadorEnCadena(loc, cad));
  loc->dato=NULL;
  loc->dato=i;
  return cad;
}


TCadena intercambiar(TLocalizador loc1, TLocalizador loc2, TCadena cad)
{
  assert(localizadorEnCadena(loc1, cad) && localizadorEnCadena(loc2, cad));
 TInfo uno = copiaInfo(loc1->dato);
liberarInfo(loc1->dato);
 loc1->dato=loc2->dato;
 
 loc2->dato=uno;

 /*
 liberarInfo();
 liberarInfo();
 */
  return cad;
}



/*
  Devuelve el primer 'TLocalizador' con el que se accede a un elemento cuyo
  componente natural es igual a 'clave', buscando desde 'loc' (inclusive) hacia
  el final de 'cad'. Si no se encuentra o 'cad' es vacía devuelve un
  'TLocalizador' no válido.
  Precondición: esVaciaCadena(cad) o localizadorEnCadena(loc, cad).
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos en 'cad'.
 */

TLocalizador siguienteClave(nat clave, TLocalizador loc, TCadena cad)
{
    assert(esVaciaCadena(cad) || localizadorEnCadena(loc, cad));
   

       for(int numerador = 99;loc!=NULL && natInfo(loc->dato)!=clave &&(numerador)>1 ;numerador--)
    {
        loc=loc->siguiente;

      }
   
    return loc;
}
/*
  Devuelve el primer 'TLocalizador' con el que se accede a un elemento cuyo
  componente natural es igual a 'clave', buscando desde 'loc' (inclusive) hacia
  el inicio de 'cad'. Si no se encuentra o 'cad' es vacía devuelve un
  'TLocalizador' no válido.
  Precondición: esVaciaCadena(cad) o localizadorEnCadena(loc, cad).
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos en 'cad'.
 */

TLocalizador anteriorClave(nat clave, TLocalizador loc, TCadena cad){
    assert(esVaciaCadena(cad) || localizadorEnCadena(loc, cad));
   

       for(int numerador = 99;loc!=NULL && natInfo(loc->dato)!=clave &&(numerador)>1 ;numerador--)
    {
        loc=loc->anterior;

      }
   
    return loc;
}

/*
  Devuelve el 'TLocalizador' con el que se accede al elemento cuyo componente
  natural es el menor en el segmento que va desde 'loc' hasta finalCadena(cad).
  Si hay más de un elemento cuyo valor es el menor el resultado accede al que
  precede a los otros.
  Precondición: localizadorEnCadena(loc, cad).
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos en 'cad'.
*/



TLocalizador menorEnCadena(TLocalizador loc, TCadena cad)
{
  assert(localizadorEnCadena(loc, cad));
  TLocalizador menor=loc;
 while (loc->siguiente!= NULL)
 {
 if (natInfo(loc->dato) <natInfo(menor->dato))
        {
          menor=loc->siguiente;
          loc=loc->siguiente;
        } else
       {
        loc=loc->siguiente;
        }
 }


 if (natInfo(loc->dato)< natInfo(menor->dato))
        {
          menor=loc;
          
        } else
       {
      
        }

return menor;

 }



