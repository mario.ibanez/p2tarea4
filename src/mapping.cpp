/*48008660*/
#include "../include/mapping.h"
#include "../include/colaDePrioridad.h"
#include "../include/conjunto.h"
#include "../include/avl.h"
#include "../include/colaAvls.h"
#include "../include/pila.h"
#include "../include/binario.h"
#include "../include/cadena.h"
#include "../include/info.h"
#include "../include/utils.h"
#include "../include/usoTads.h"
#include "../include/iterador.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
struct repMap {
    nodo** hash;
    nat tamTabla;//tamaño maximo de la tabla
    nat cantElems;// cantidad de elementos que la tabla tiene
};
struct nodo {
    nat clavenodo;
    double valor;
    nodo* sig;
};

typedef struct nodo *TNodo;

nat hashtexto(nat w,nat N);
bool estaEnCadena (TNodo nodito,nat clave);
/*
Crea un 'TMapping' vacío (sin elementos) de asociaciones nat -> double.
Podrá haber hasta M asociaciones.
El tiempo de ejecución en el peor caso es O(M).
*/
TMapping crearMap(nat M){
TMapping t= new repMap;

t->hash = new  nodo * [M];

//nodo * solucion_h = new nodo [M];
  //  t->hash=&solucion_h;
    for (nat i=0; i<M;i++){
        t->hash [i]=NULL;
        }
        t->tamTabla= M;
        t->cantElems=0;
return t;
 }
/*
Inserta en 'map' la asociación entre 'clave' y 'valor'.
Precondición: !estaLlenoMap(map) y !existeAsociacionEnMap(clave, map).
Devuelve 'map'.
El tiempo de ejecución en el peor caso es O(1).
*/
TMapping asociarEnMap(nat clave, double valor, TMapping map){
assert(!estaLlenoMap(map) && !existeAsociacionEnMap(clave, map));
    nat pos = hashtexto(clave,map->tamTabla);
    nodo*nuevo = new nodo;
    nuevo->clavenodo = clave;
    nuevo->valor = valor;
    nuevo->sig= map->hash[pos];
    map->hash[pos] = nuevo;
    map->cantElems= map->cantElems+1;
return map;
}
/*
Elimina de 'map' la asociación correspondiente a 'clave' y libera la
menoria asignada a mantener esa asociación.
Precondición: existeAsociacionenMap(clave, h).
Devuelve 'map'.
El 'TMapping' resultado comparte memoria con el parámetro'.
El tiempo de ejecución es O(1) en promedio.
*/
TMapping desasociarEnMap(nat clave, TMapping map){
assert( existeAsociacionEnMap(clave, map));
    nat pos = hashtexto(clave,map->tamTabla);
    nodo * iter= map->hash[pos];
    while (iter ->sig !=NULL)
        {
            if (iter->sig->clavenodo==clave)
            {
                nodo * eliminar = iter->sig;
                iter->sig=iter->sig->sig;
                delete eliminar;
            }
        else
            {
        iter=iter->sig;
            }
        }
if (map->hash[pos]->clavenodo==clave)
{
    nodo * eliminar = map->hash[pos];
    map->hash[pos]= eliminar->sig;
    delete eliminar;
}
map->cantElems= map->cantElems-1;
return map;
}
/*
Devuelve 'true' si y solo si en 'map' hay una asociación correspondiente
a 'clave'.
El tiempo de ejecución es O(1) en promedio.
*/
bool existeAsociacionEnMap(nat clave, TMapping map){
nat pos = hashtexto(clave,map->tamTabla);
return (map->hash[pos]!=NULL &&  estaEnCadena(map->hash[pos],clave) );
}
/*
Devuelve el valor correspondiente a la asociacion de 'clave' en 'h'.
Precondición: existeAsociacionenMap(clave, h).
El tiempo de ejecución es O(1) en promedio.
*/
double valorEnMap(nat clave, TMapping map){
assert(existeAsociacionEnMap(clave, map));
nat pos = hashtexto(clave,map->tamTabla);
nodo * iter = map->hash[pos];
bool encontre = false;
while (!encontre)
{
if (iter->clavenodo==clave)
encontre = true;
else
iter=iter->sig;
}
return iter->valor;
}
/*
Devuelve 'true' si y solo si 'map' tiene 'M' elementos, siendo 'M' el
parámetro pasado en crearMap.
El tiempo de ejecución es O(1).
*/
bool estaLlenoMap(TMapping map){
return (map->cantElems==map->tamTabla);
}
/*
Libera la memoria asignada a 'map' y todos sus elementos.
El tiempo de ejecución en el peor caso es O(M), siendo 'M' el parámetro
pasado en crearMap.
*/
void liberarMap(TMapping map){
    for (nat i=0;i<map->tamTabla;i++)
    {  
         while (map->hash[i]!=NULL){
         TNodo a_borrar=map->hash[i];
         map->hash[i]=  map->hash[i]->sig;
         delete a_borrar;
         }
   // delete map->hash[i];
    }
//map->hash=NULL;
delete [] map->hash;
delete map;
}

//*****************************************************************************************//
nat hashtexto(nat w,nat N){
    /*
nat res=0;
for (nat i=0;i<w;i++)
res=res + w ;
*/

return w%N;
}

bool estaEnCadena (TNodo nodito,nat clave){
    bool value=false;
    while (nodito!=NULL){
       if(nodito->clavenodo==clave)
            value=true;
        nodito=nodito->sig;

    }

    return value;
}