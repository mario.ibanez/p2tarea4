/*48008660*/
#include "../include/iterador.h"
#include "../include/binario.h"

#include "../include/utils.h"


#include <assert.h>
#include <stdio.h>


typedef struct nodoIt *PNodoIt;
struct nodoIt {
  nat elem;
  PNodoIt sig;
};

struct repIterador 
{
   PNodoIt inicio,fin,actual;
   bool bloqueado;
};


/*
  Crea un 'TIterador' vacío (sin elementos) cuya posición actual no está
  definida.
  Se le puede agregar elementos.
  El tiempo de ejecución en el peor caso es O(1).
 */
TIterador crearIterador(){
TIterador res = new repIterador;
res->actual= res->inicio= res->fin=NULL;
res->bloqueado= false;
return res;
}// crearIterador

//void liberarnodo(PNodoIt nodo);


//******************************** ARRANCO ACA ****************************************************************
/*
  Agrega 'elem' al final de 'iter' si y solo si nunca se reinicio.
  Devuelve 'iter'.
  La posición actual sigue indefinida.
  El tiempo de ejecución en el peor caso es O(1).
 */
PNodoIt crearNodo (nat elem){
  PNodoIt nuevo= new nodoIt;
  nuevo->elem=elem;
  nuevo->sig=NULL;
 return nuevo;
}


TIterador agregarAIterador(nat elem, TIterador iter){
  if (iter->bloqueado==false) {
  
    
  if(iter->inicio==NULL){
     iter->inicio=iter->fin=crearNodo(elem);
   
    } else{
   PNodoIt nuevo=crearNodo(elem);
    iter->fin->sig=nuevo;
    iter->fin=nuevo;
    nuevo->sig=NULL;

        }

  }
  return iter;
}

/*
  Mueve la posición actual de 'iter' al primero si y solo si 'iter' tiene
  elementos.
  Devuelve 'iter'.
  No se podrán agregar más elementos (aunque no hubiera ninguno).
  El tiempo de ejecución en el peor caso es O(1).
 */
TIterador reiniciarIterador(TIterador iter){
  if(iter->inicio!=NULL){
      iter->actual=iter->inicio;
      iter->bloqueado=true;
      return iter;
    
  } else {
   iter->bloqueado=true;
   // iter->actual=iter->inicio;
    return iter;}
}

/*
  Avanza la posición actual de 'iter' hacia el siguiente si y solo si'
  estaDefinidaActual(iter).
  Devuelve 'iter'.
  Si la posición actual es la última posición entonces la posición actual  
  queda indefinida.
  El tiempo de ejecución en el peor caso es O(1).
 */
TIterador avanzarIterador(TIterador iter){
  assert( estaDefinidaActual(iter));
  iter->actual=iter->actual->sig;
    return iter;
}

/*
  Devuelve el elemento de la posición actual de 'iter'.
  Precondición: estaDefinidaActual(iter).
  El tiempo de ejecución en el peor caso es O(1).
 */
nat actualEnIterador(TIterador iter){
  assert(estaDefinidaActual(iter));

    return iter->actual->elem;
  
}

/*
  Devuelve 'true' si y solo si la posición actual de 'iter' está definida.
  El tiempo de ejecución en el peor caso es O(1).
 */
bool estaDefinidaActual(TIterador iter){
    return (iter->actual!=NULL);

}

/*
  Libera la memoria asignada a 'iter'.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad
  de elementos de 'iter'.
 */



void liberarIterador(TIterador iter){

while (iter->inicio!=iter->fin){
PNodoIt puente=iter->inicio;
iter->inicio= iter->inicio->sig;

delete puente;

 }
 if(iter->inicio!=NULL){
 delete iter->inicio;

 }

delete iter; 
}
/*

void liberarIterador(TIterador iter){

while (iter->inicio!=iter->fin){
PNodoIt puente= new nodoIt;
puente=iter->inicio;
iter->inicio= iter->inicio->sig;
puente->elem=0;
puente->sig=NULL;
delete puente;

 }
 if(iter->inicio!=NULL){
    iter->inicio->elem=0;
    iter->inicio->sig=NULL;

 }
 iter->inicio=NULL;
 iter->bloqueado=NULL;  
 iter->actual=NULL;
 iter->fin=NULL;
delete iter; 
}
*/