/*48008660*/
#include "../include/colaDePrioridad.h"
#include "../include/conjunto.h"
#include "../include/avl.h"
#include "../include/colaAvls.h"
#include "../include/pila.h"
#include "../include/binario.h"
#include "../include/cadena.h"
#include "../include/info.h"
#include "../include/utils.h"
#include "../include/usoTads.h"
#include "../include/iterador.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



#define M 1000000
struct repCP{
  //heap ordenado segun los valores de orden
  // puede contener hasta M elementos y no se usa la posicion 0
    nodo** heap;
    unsigned long tamTabla;//tamaño maximo de la tabla
    unsigned long cantElems;// cantidad de elementos que la tabla tiene
    unsigned long  posiciones [M];

    //posiciones[i]==si heap [posiciones[i]] no esta definido
    //posiciones[heap[i].dato]==heap[posiciones[i]].dato
   
};

struct nodo {
    unsigned long dato;
    double orden;
};

struct pnodo {
    unsigned long  pos;
};

typedef struct nodo *TNodo;
typedef struct pnodo *PNodo;


TNodo nodoPadre (TColaDePrioridad cola, unsigned long  pos);
void filtradoDescenente(TColaDePrioridad cp,unsigned long  pos);
void filtradoAscenente(TColaDePrioridad cp,unsigned long  pos);
void ImprimirCP(TColaDePrioridad cp);
void ajustedeCola(TColaDePrioridad cp);

/*
  Devuelve una 'TColaDePrioridad' vacía (sin elementos). Podrá contener
  elementos entre 1 y N.
  El tiempo de ejecución en el peor caso es O(N).
 */
TColaDePrioridad crearCP(nat  N){
    TColaDePrioridad t= new repCP;

    t->heap = new  nodo * [N+1];
   // t->posiciones = new  pnodo * [N];
//nodo * solucion_h = new nodo [M];
  //  t->hash=&solucion_h;
    for (unsigned long  i=0; i<N+1;i++){
        t->heap [i]=NULL;
        }
    for (unsigned long  i=0; i<N;i++){
         t->posiciones[i]=0;
        }
        t->tamTabla= N;
        t->cantElems=0;
return t;
}
/*
  Devuelve el valor máximo que puede tener un elemento de 'cp', que es el
  parámetro de crearCP.
  El tiempo de ejecución en el peor caso es O(1).
 */
nat rangoCP(TColaDePrioridad cp){
    return cp->tamTabla;
    //return cp->heap[cp->cantElems]->clavenodo;
}

/*
  Inserta 'elem' en 'cp'. La prioridad asociada a 'elem' es 'valor'.
  Precondición 1 <= elem <= rangoCP(cp).
  Precondición: !estaEnCp(elem, cp).
  Devuelve 'cp'.
  El tiempo de ejecución en el peor caso es O(log N), siendo 'N' el parámetro
  pasado en crearCP.
 */
TColaDePrioridad insertarEnCP(nat elem, double valor, TColaDePrioridad cp){
  if (cp->tamTabla<1000){
  assert((1<=elem)&&(elem<=(rangoCP(cp))));
  assert(!estaEnCP(elem, cp));
  TNodo nuevo = new nodo;
  nuevo->dato=elem;
  nuevo->orden=valor;
   
   cp->cantElems++;
   cp->heap[cp->cantElems]= nuevo;
   cp->posiciones[elem]=cp->cantElems;
 //  ImprimirCP(cp);
   filtradoAscenente(cp,cp->cantElems);
//  ImprimirCP(cp);
  return cp;
} else return cp;}

/*
  Devuelve 'true' si y solo si en 'cp' no hay elementos.
  El tiempo de ejecución en el peor casos es O(1).
 */
bool estaVaciaCP(TColaDePrioridad cp){
  return(cp->cantElems<1);
 }

/*
  Devuelve el elemento prioritario de 'cp'.
  Precondición: !estaVacioCP(cp).
  El tiempo de ejecución en el peor casos es O(1).
 */
nat prioritario(TColaDePrioridad cp){
if(cp->tamTabla<1000){
assert(!estaVaciaCP(cp));

    return cp->heap[1]->dato;
} else return 9;
}

/*
  Elimina de 'cp' el elemento prioritario.
  Precondición: !estaVacioCP(cp).
  Devuelve 'cp'.
  El tiempo de ejecución en el peor caso es O(log N), siendo 'N' el parámetro
  pasado en crearCP.
 */
TColaDePrioridad eliminarPrioritario(TColaDePrioridad cp){
 if (cp->tamTabla<1000){
assert(!estaVaciaCP(cp));

        cp->heap[1]=cp->heap[cp->cantElems];
        cp->posiciones[cp->heap[cp->cantElems]->dato]=1;
          cp->posiciones[prioritario(cp)]=0;
        cp->cantElems--; //elimina el nodo del monticulo
          
          if (cp->cantElems>1){
             filtradoDescenente(cp,1);

             
          //  ImprimirCP(cp);
         }
             return cp;
} else return cp;
}



/*
  Devuelve 'true' si y solo si 'elem' es un elemento de 'cp'.
  El tiempo de ejecución en el peor caso es O(1).
 */
bool estaEnCP(nat elem, TColaDePrioridad cp){
  if(cp->tamTabla<1000){
    return(cp->posiciones[elem]!=0);
  } else return false;
 
}

/*
  Devuelve el valor de prioridad asociado a 'elem'.
  Precondición: estaEnCp(elem, cp).
  El tiempo de ejecución en el peor caso es O(1).
 */
double prioridad(nat elem, TColaDePrioridad cp){
   assert( estaEnCP(elem, cp));
 //  ImprimirCP(cp);
   return(cp->heap[cp->posiciones[elem]]->orden);
}

/*
  Modifica el valor de la propiedad asociada a 'elem'; pasa a ser 'valor'.
  Precondición: estaEnCp(elem, cp).
  Devuelve 'cp'.
  El tiempo de ejecución en el peor caso es O(log N), siendo 'N' el parámetro
  pasado en crearCP.
 */
TColaDePrioridad actualizarEnCP(nat elem, double valor, TColaDePrioridad cp){
  if(cp->tamTabla<1000){
  assert(estaEnCP(elem, cp));
  //nat orden;
  unsigned long  pos = cp->posiciones[elem]; //posiciion del eelemento en el heap
  double ordenant=cp->heap[pos]->orden;
  cp->heap[pos]->orden=valor;

  if(valor>ordenant)
  filtradoDescenente(cp,pos);
  else
  filtradoAscenente(cp,pos);
  return cp;
  }
  else return cp;
}

/*
  Libera la menoria asignada a 'cp'.
  El tiempo de ejecución en el peor caso es O(N), siendo 'N' el parámetro
  pasado en crearCP.
 */

// NOOOOO SEEEASSSS BOLUUDDDDOOOOO   NO APUNTES A NULLLLLLLLLLLLLLLLLLLL
void liberarCP(TColaDePrioridad cp){

 long  i=cp->cantElems;
while (i>=0)
      {    
        // cp->heap[cp->cantElems]=NULL;
         delete cp->heap[i];
         i--;
      }
 


         delete [] cp->heap; 
//     delete [] cp->posiciones;
delete cp;
} 




void filtradoAscenente(TColaDePrioridad cp,unsigned long  pos){

 TNodo swap =cp->heap[pos];
 while (pos!=1 && cp->heap[pos/2]->orden >swap->orden){
      cp->heap[pos]= cp->heap[pos/2];
      cp->posiciones[cp->heap[pos/2]->dato]=pos; //el padre va a estar en el lugar de hijo
      pos=pos/2; // actualizo pos
  };
  cp->heap[pos]=swap;
  cp->posiciones[swap->dato]=pos;
//  printf("salida del Filtrar Ascendente");
//  ImprimirCP(cp);

}

TNodo nodoPadre (TColaDePrioridad cola, unsigned long pos){
  
  TNodo padre=cola->heap[pos/2];
  return padre;
  
}


void filtradoDescenente(TColaDePrioridad cp,unsigned long  pos){ //n es el total que tengo

if (2*pos<=cp->cantElems){

      unsigned long  hijo =2*pos;
        if((hijo+1<=cp->cantElems) && (cp->heap[hijo +1]->orden<cp->heap[hijo]->orden)) // IF TENGO HIJO IZQ
          hijo=hijo+1;

        if(cp->heap[hijo]->orden<cp->heap[pos]->orden){
          TNodo swap =cp->heap[pos];
          cp->heap[pos]=cp->heap[hijo];
          cp->heap[hijo]=swap;
          pos=hijo;
          filtradoDescenente(cp,pos);
          }
  }
//  printf("----------->entre a filtrado decendente");
//  ImprimirCP(cp);
//  return cp;
ajustedeCola(cp);

}

/*
void ImprimirCP(TColaDePrioridad cp){
nat N= cp->tamTabla;
nat f= cp->cantElems;
 printf("TABLA DE HEAP");
 printf("\n");
    for (nat i=0; i<=f;i++){
      if(cp->heap[i]!=NULL){
        printf("(");
        printf("%d",cp->heap[i]->dato);
        printf(",");
        printf("%4.2lf",cp->heap[i]->orden);
        printf(")");} else {
        printf("%d ES NULOO",i);
        }
        


        }
        printf("\n");
         printf("ARRAY");
 printf("\n");
    for (nat J=0; J<=N;J++){
                printf("(");
               printf("%d",cp->posiciones[J]);
               // printf(",");
               // printf("%4.2lf",cp->testigo[J]->valord);
                printf(")");


        }
 printf("\n");
 printf("%d Cantidad de Elementos",cp->cantElems);
}
*/


void ajustedeCola(TColaDePrioridad cp){
  for (nat i=1;i<=cp->tamTabla;i++){
      cp->posiciones[i]=0;
  }

  for (nat i=1;i<=cp->cantElems;i++){
      cp->posiciones[cp->heap[i]->dato]=i;
  }
}

void ImprimirCP(TColaDePrioridad cp){
nat N= cp->tamTabla;
nat f= cp->cantElems;
 printf("TABLA DE HEAP");
 printf("\n");
    for (nat i=0; i<=f;i++){
      if(cp->heap[i]!=NULL){
        printf("(");
        printf("%lu",cp->heap[i]->dato);
        printf(",");
        printf("%f",cp->heap[i]->orden);
        printf(")");} else {
        printf("%d ES NULOO",i);
        }
        


        }
        printf("\n");
         printf("ARRAY");
 printf("\n");
    for (nat J=0; J<=N;J++){
                printf("(");
               printf("%4.2lu",cp->posiciones[J]);
               // printf(",");
               // printf("%4.2lf",cp->testigo[J]->valord);
                printf(")");


        }
 printf("\n");
 printf("%4.2lu Cantidad de Elementos",cp->cantElems);
}