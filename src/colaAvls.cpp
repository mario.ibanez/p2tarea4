/*48008660*/
#include "../include/colaAvls.h"
#include "../include/pila.h"
#include "../include/binario.h"
#include "../include/cadena.h"
#include "../include/info.h"
#include "../include/utils.h"
#include "../include/usoTads.h"
#include "../include/iterador.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct nodo *Tndo;

 struct repColaAvls{
    nodo* final;
    nodo* inicio;
    nat largo;
    
 };

struct nodo {
TAvl dato ;
nodo * sig ;
nodo * ant ;
};


/*
  Devuelve una 'TColaAvls' vacía (sin elementos).
  El tiempo de ejecución en el peor caso es O(1).
 */
TColaAvls crearColaAvls(){
    TColaAvls nueva = new repColaAvls;
    nueva->final=NULL;
    nueva->inicio=NULL;
    nueva->largo=0;
    return nueva;
}

Tndo crearNodo (TAvl avl){
Tndo nuevo = new  nodo;
nuevo->dato=avl;
nuevo->sig=NULL;
nuevo->ant=NULL;
return nuevo;
}

void liberarNodo (Tndo borrar){
 liberarAvl(borrar->dato); 
 borrar->sig=NULL;
 borrar->ant=NULL;
 delete  borrar->sig;
 delete borrar->ant;
 delete borrar;

}
/*
  Encola 'avl' en 'c'.
  Devuelve 'c'.
  El tiempo de ejecución en el peor caso es O(1).
 */
TColaAvls encolar(TAvl avl, TColaAvls c){
   if (c->largo==0){
     Tndo nuevo=crearNodo(avl);
     c->largo=c->largo+1;
     c->final=nuevo;
     c->inicio=nuevo;
     return c;
   } else {
     Tndo nuevo=crearNodo(avl);
     c->largo=c->largo+1;
     nuevo->sig=c->inicio;
     c->inicio->ant=nuevo;
     c->inicio=nuevo;
     return c;
   }
    
}

/*
  Remueve de 'c' el elemento que está en el frente.
  Si estaVaciaColaAvls(c) no hace nada.
  Devuelve 'c'.
  NO libera la memoria del elemento removido.
  El tiempo de ejecución en el peor caso es O(1).
 */
TColaAvls desencolar(TColaAvls c){
  if (c==NULL) {return c;}
  else if (c->largo==1){
    Tndo borrar =c->final;
     c->largo=c->largo-1;
    
     c->final->sig=NULL;
     c->final->ant=NULL;
     c->final=NULL;
     c->inicio=NULL;
     delete borrar;
     return c;
   } else {
     Tndo borrar =c->final;
     c->largo=c->largo-1;
     c->final->ant->sig=NULL;
     c->final=c->final->ant;
    delete borrar;
     return c;
   }
}

/*
  Libera la memoria asignada a 'c', pero NO la de sus elementos.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad
  de elementos de 'c'.
 */
void liberarColaAvls(TColaAvls c){
  
    delete c;

}
    


/*
  Devuelve 'true' si y solo si 'c' es vacía (no tiene elementos).
  El tiempo de ejecución en el peor caso es O(1).
 */
bool estaVaciaColaAvls(TColaAvls c){
    return (c->largo==0);
}

/*
  Devuelve el elemento que está en el frente de 'c'.
  Precondición: ! estaVaciaColaAvls(c);
  El tiempo de ejecución en el peor caso es O(1).
 */
TAvl frente(TColaAvls c){
  assert(!estaVaciaColaAvls(c));
    return c->final->dato;
}


