
/*48008660*/
#include "../include/pila.h"
#include "../include/binario.h"
#include "../include/cadena.h"
#include "../include/info.h"
#include "../include/utils.h"
#include "../include/usoTads.h"
#include "../include/iterador.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


typedef struct nodo *Tndo;

struct repPila {
    nodo* cima;
    nat altura;
    nat tamanio;
};

struct nodo {
int  dato ;
nodo * sig ;
nodo * ant ;
};

/*
  Devuelve una 'TPila' vacía (sin elementos) que puede tener hasta 'tamanio'
  elementos.
  Precondición: tamanio > 0.
 */
TPila crearPila(nat tamanio){
    assert(tamanio>0);
    TPila nueva = new repPila;
    nueva->cima=NULL;
    nueva->altura=0;
    nueva->tamanio=tamanio;
    return nueva;
}

/*
  Apila 'num' en 'p'.
  Devuelve 'p'.
  Si estaLenaPila(p) no hace nada.
  El tiempo de ejecución en el peor caso es O(1).
 */
TPila apilar(nat num, TPila p){
  if (p->tamanio==p->altura){
    return p;
  } else{
  Tndo nuevo = new  nodo;
  p->altura=p->altura + 1;
  nuevo->ant=p->cima;
  p->cima=nuevo;
  nuevo->dato=num;
  nuevo->sig=NULL;
  return p;
  }
}

/*
  Remueve de 'p' el elemento que está en la cima.
  Devuelve 'p'.
  Si estaVaciaPila(p) no hace nada.
  El tiempo de ejecución en el peor caso es O(1).
 */
TPila desapilar(TPila p){
 if (p->altura==0){
    return p;
  } else{
    Tndo borrar=p->cima;
    p->cima=p->cima->ant;
    p->altura=p->altura-1;
    delete borrar;
    return p;
  }
}

/*
  Libera la memoria asignada a 'p'.
 */
void liberarPila(TPila p){
  while (p->altura>0){
  desapilar(p);
}
delete p;
}

/*
  Devuelve 'true' si y solo si 'p' es vacía (no tiene elementos).
  El tiempo de ejecución en el peor caso es O(1).
 */
bool estaVaciaPila(TPila p){
return (p->altura==0);
}

/*
  Devuelve 'true' si y solo si la cantidad de elementos en 'p' es 'tamanio'
  (siendo 'tamanio' el valor del parámetro con que fue creada 'p').
  El tiempo de ejecución en el peor caso es O(1).
 */
bool estaLlenaPila(TPila p){
    return (p->altura==p->tamanio);
}

/*
  Devuelve el elemento que está en la cima de 'p'.
  Precondición: ! estaVaciaPila(p);
  El tiempo de ejecución en el peor caso es O(1).
 */
nat cima(TPila p){
  assert(! estaVaciaPila(p));
return p->cima->dato;
}
