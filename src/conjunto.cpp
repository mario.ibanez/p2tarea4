/*48008660*/
#include "../include/conjunto.h"
#include "../include/avl.h"
#include "../include/colaAvls.h"
#include "../include/pila.h"
#include "../include/binario.h"
#include "../include/cadena.h"
#include "../include/info.h"
#include "../include/utils.h"
#include "../include/usoTads.h"
#include "../include/iterador.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

struct repConjunto {
       TAvl conjunto;
};

TConjunto copiaDeConjunto(TConjunto c1);
void imprimrIterador(TIterador i);
void imprimirConjunto(TConjunto c1);

/*
  Devuelve un 'TConjunto' vacío (sin elementos).
  El tiempo de ejecución en el peor caso es O(1).
 */
TConjunto crearConjunto(){
   TConjunto single= new  repConjunto;
   single->conjunto=crearAvl();
   return single;
}

/*
  Devuelve un 'TConjunto' cuyo único elemento es 'elem'.
  El tiempo de ejecución en el peor caso es O(1).
 */
TConjunto singleton(nat elem){
   TConjunto single= crearConjunto();
   single->conjunto=insertarEnAvl(elem,single->conjunto);
  return single;
}

/*
  Devuelve un 'TConjunto' con los elementos que pertenecen a 'c1' o 'c2'.
  El 'TConjunto' resultado no comparte memoria ni con 'c1' no con 'c2'.
  El tiempo de ejecucion en el peor caso es O(n),
  siendo  'n' la cantidad de elementos del 'TConjunto' resultado.
 */
TConjunto unionDeConjuntos(TConjunto c1, TConjunto c2){
    if(estaVacioConjunto(c1) && estaVacioConjunto(c2)){
      TConjunto nuevo =crearConjunto();
      return nuevo;}
    if(!estaVacioConjunto(c1) && estaVacioConjunto(c2)){
      TConjunto nuevoconjunto=copiaDeConjunto(c1);
      return nuevoconjunto;
    }
      if(estaVacioConjunto(c1) && !estaVacioConjunto(c2)){
      TConjunto nuevoconjunto=copiaDeConjunto(c2);
      return nuevoconjunto;
    } else {




    TConjunto nuevoconjunto=crearConjunto();
    TIterador nuevo = iteradorDeConjunto(c1);
    nat n=0;
    nuevo=reiniciarIterador(nuevo);
    n=actualEnIterador(nuevo);
    while(estaDefinidaActual(nuevo)){
       
       n=actualEnIterador(nuevo);
       nuevoconjunto->conjunto=insertarEnAvl(n,nuevoconjunto->conjunto);
       nuevo=avanzarIterador(nuevo);
    }

    TIterador nuevo2 = iteradorDeConjunto(c2);
    nuevo2=reiniciarIterador(nuevo2);
    n=actualEnIterador(nuevo2);

   while(estaDefinidaActual(nuevo2)){
        n=actualEnIterador(nuevo2);
         if(!perteneceAConjunto(n,nuevoconjunto)){
                  
                   nuevoconjunto->conjunto=insertarEnAvl(n,nuevoconjunto->conjunto);
                  
          } else {
            
             
          }
           nuevo2=avanzarIterador(nuevo2); 
          
    }
  
liberarIterador(nuevo);
liberarIterador(nuevo2);
//imprimirConjunto(nuevoconjunto);
return nuevoconjunto;
     }
}





/*
  Devuelve un 'TConjunto' con los elementos de 'c1' que no pertenecen a 'c2'.
  El 'TConjunto' resultado no comparte memoria ni con 'c1' no con 'c2'.
  El tiempo de ejecucion en el peor caso es O(n1 + n2),
  siendo 'n1' y 'n2' la cantidad de elementos de 'c1' y 'c2' respectivamente y
  'n' la del 'TConjunto' resultado.
 */
TConjunto diferenciaDeConjuntos(TConjunto c1, TConjunto c2){
 // imprimirConjunto(c1);
//  imprimirConjunto(c2);
 TConjunto unionConjunto = unionDeConjuntos(c1,c2);
 TIterador unionIterador = iteradorDeConjunto(unionConjunto);
 TConjunto nuevoConjunto = crearConjunto();
 //imprimrIterador(unionIterador);
 nat n=0;

 if(!estaDefinidaActual(unionIterador)){
   return nuevoConjunto;

 } else {
 unionIterador=reiniciarIterador(unionIterador);
 n=actualEnIterador(unionIterador);
 while(estaDefinidaActual(unionIterador)){
       n=actualEnIterador(unionIterador);
       
      if((perteneceAConjunto(n,c1)&&!perteneceAConjunto(n,c2)))
        {
       nuevoConjunto->conjunto=insertarEnAvl(n,nuevoConjunto->conjunto);
        }
       unionIterador=avanzarIterador(unionIterador);
    }
liberarIterador(unionIterador);
liberarConjunto(unionConjunto);
//imprimirConjunto(nuevoConjunto);
 return nuevoConjunto;
}
}

/*
  Devuelve 'true' si y solo si 'elem' es un elemento de 'c'.
  El tiempo de ejecución en el peor caso es O(log n), siendo 'n' la cantidad de
  elementos de 'c'.
 */
bool perteneceAConjunto(nat elem, TConjunto c){
   return(!(NULL==buscarEnAvl(elem,c->conjunto))); 
}

/*
  Devuelve 'true' si y solo si 'c' es vacío (no tiene elementos).
  El tiempo de ejecución en el peor caso es O(1).
 */
bool estaVacioConjunto(TConjunto c){
    return(c->conjunto==NULL);
}

/*
  Devuelve la cantidad de elementos de 'c'.
  El tiempo de ejecución en el peor caso es O(1).
 */
nat cardinalidad(TConjunto c){
  if (c==NULL){ return 0;} else
  {
   nat n=cantidadEnAvl(c->conjunto);
    return n;

  }
  
}

/*
  Devuelve un 'TConjunto' con los 'n' elementos que están en en el rango
  [0 .. n - 1] del arreglo 'elems'.
  Precondiciones:
  - n > 0
  - Los elementos están ordenados de manera creciente estricto
  (creciente y sin repetidos).
  El tiempo de ejecución en el peor caso es O(n).
 */


TConjunto arregloAConjunto(nat *elems, nat n){
   //ArregloNats tope= elems;
   //leerNat();
    TConjunto nuevo=crearConjunto();
    for (nat i=0;i<=n-1;i++){
     nuevo->conjunto= insertarEnAvl(elems[i] ,nuevo->conjunto);
    }
    return nuevo;
}

/*
  Devuelve un 'TIterador' de los elementos de 'c'.
  En la recorrida del iterador devuelto los elementos aparecerán en orden
  creciente.
  El 'TIterador' resultado no comparte memoria con 'c'.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos de 'c'.
 */
TIterador iteradorDeConjunto(TConjunto c){
  if (estaVacioConjunto(c)){
   TIterador nuevo=crearIterador();
   return nuevo;
   }
  TIterador nuevo= enOrdenAvl(c->conjunto);
  nuevo=reiniciarIterador(nuevo);
  return nuevo;
}

/*
  Libera la memoria asignada a 'c' y la de todos sus elementos.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos de 'c'.
 */
void liberarConjunto(TConjunto c){
    liberarAvl(c->conjunto);
    delete c;
}

//**************************************************************************************




TConjunto copiaDeConjunto(TConjunto c1){
 TIterador nuevo= iteradorDeConjunto(c1);
 TConjunto nuevoconjunto=crearConjunto();

    nat n=0;
    nuevo=reiniciarIterador(nuevo);
    n=actualEnIterador(nuevo);
    while(estaDefinidaActual(nuevo)){
       
       n=actualEnIterador(nuevo);
       nuevoconjunto->conjunto=insertarEnAvl(n,nuevoconjunto->conjunto);
       nuevo=avanzarIterador(nuevo);
    }

    liberarIterador(nuevo);
    return nuevoconjunto;

}

//ojooo imprimir iterador
void imprimrIterador(TIterador i){
  i=reiniciarIterador(i);
      if (!estaDefinidaActual(i)) {
        printf("NO ENCONTRE QUE CORNO IMPIRMIR.\n");
      } else {
        do {
          printf("%d ", actualEnIterador(i));
          i = avanzarIterador(i);
        } while (estaDefinidaActual(i));
        printf("\n");
      }
      //liberarIterador(i);
      
      
}

void imprimirConjunto(TConjunto c1){
 TIterador conj= iteradorDeConjunto(c1);
  imprimrIterador(conj);

}


TConjunto complementoAinterseccionB(TConjunto c1, TConjunto c2){
  //imprimirConjunto(c1);
  // imprimirConjunto(c2);
 TConjunto unionConjunto = unionDeConjuntos(c1,c2);
 TIterador unionIterador = iteradorDeConjunto(unionConjunto);
 TConjunto nuevoConjunto = crearConjunto();
 //imprimrIterador(unionIterador);
 nat n=0;
 unionIterador=reiniciarIterador(unionIterador);
 n=actualEnIterador(unionIterador);
 while(estaDefinidaActual(unionIterador)){
       n=actualEnIterador(unionIterador);
       
      if((perteneceAConjunto(n,c1)&&!perteneceAConjunto(n,c2))||(!perteneceAConjunto(n,c1)&&perteneceAConjunto(n,c2)))
        {
       nuevoConjunto->conjunto=insertarEnAvl(n,nuevoConjunto->conjunto);
        }
       unionIterador=avanzarIterador(unionIterador);
    }

 return nuevoConjunto;

}