/*48008660*/
#include "../include/avl.h"
#include "../include/colaAvls.h"
#include "../include/pila.h"
#include "../include/binario.h"
#include "../include/cadena.h"
#include "../include/info.h"
#include "../include/utils.h"
#include "../include/usoTads.h"
#include "../include/iterador.h"
#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <time.h>






/*
  Devuelve 'true' si y solo si en 'cad' hay un elemento cuyo campo natural es
  'elem'.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos de 'cad'.
*/
bool pertenece(nat elem, TCadena cad){
  return ( siguienteClave(elem,inicioCadena(cad),cad)!=NULL && !esVaciaCadena(cad));
}

/*
  Devuelve la cantidad de elementos de 'cad'.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos de 'cad'.
*/
nat longitud(TCadena cad){
int contador=0;
  if (esVaciaCadena(cad)){
    contador= 0;
  } else {
   TLocalizador loc = inicioCadena(cad); 
           for  (int i=0;loc!=NULL && i<100;i++)
           {
              contador=contador+1;
              loc=siguiente(loc,cad);
           }
        }
  return contador;
}


void mostrarArbol2 (TBinario arbol,nat cont, nat  l,TCadena cad);
/*
  Devuelve 'true' si y solo si 'cad' está ordenada de forma no dereciente
  (creciente de manera no estricta) según las campos naturales de sus elementos.
  Si esVaciaVadena(cad) devuelve 'true'.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos de 'cad'.
*/
bool estaOrdenadaPorNaturales(TCadena cad){
  int variante=0;
  if (esVaciaCadena(cad)){
   variante=0;
  } else {
    TLocalizador loc = finalCadena(cad); 
 //   printf("\n %d longitud de cad-------->",longitud(cad)); 
    for (nat i=0; i<longitud(cad);i++)
      {

      if(menorEnCadena(loc,cad)!=loc){variante=variante+1;};
 //    printf("\n %d entre al if y valor de variante-------->",variante); 
  //    printf("\n %d entre al if y valor de i-------->",variante); 

      loc=anterior(loc,cad);
      }
  }
  
return (variante==0);

}
 
/*
  Devuelve 'true' si y solo si los componentes naturales de algún par de 
  elementos de 'cad' son iguales.  
*/
bool hayNatsRepetidos(TCadena cad){
  
  
  nat variante=0;
   TCadena Cadena3= copiarSegmento(inicioCadena(cad),finalCadena(cad),cad);
 
  if (longitud(cad)<=1){
   variante=longitud(cad);
  } else {
    for(nat j=1;j<=99;j++)// hay que buscar una forma mas eficiente de hacer esto<--------------
            {
            
            if( siguienteClave(j,inicioCadena(Cadena3),Cadena3)!=NULL){
// printf("\n ENTRE A NATS Rif------->  ");
                variante=variante+1;

            } else{ variante=variante;
            }
          

            }
                   
        

  
  }
 liberarCadena(Cadena3);
  return  !(variante==longitud(cad));
}


/*
  Devuelve 'true' si y solo si 'c1' y 'c2' son iguales (es decir, si los
  elementos son iguales y en el mismo orden).
  Si esVaciaCadena(c1) y esVaciaCadena(c2) devuelve 'true'.
  El tiempo de ejecución en el peor caso es O(n1 + n2), siendo 'n1' u 'n2' la
  cantidad de elementos de 'c1' y 'c2' respectivamente.
*/
bool sonIgualesCadena(TCadena c1, TCadena c2){
  int valgrin=0;
 // TInfo sac= crearInfo(22,2);
 // TLocalizador set =inicioCadena(c1);
                       TCadena cadena1=copiarSegmento(inicioCadena(c1),finalCadena(c1),c1);
                      TLocalizador set1 =finalCadena(cadena1);
                      TCadena cadena2=copiarSegmento(inicioCadena(c2),finalCadena(c2),c2);
                      TLocalizador set2 =finalCadena(cadena2);
if(esVaciaCadena(c1) && esVaciaCadena(c2)) {
      valgrin=0;
    } else if ((!esVaciaCadena(c1) && esVaciaCadena(c2))||(esVaciaCadena(c1) && !esVaciaCadena(c2)) )
           {
              valgrin=100;
           } else if(longitud(c1)==longitud(c2))
                { 

                   

                      for(nat i=0;i<=longitud(c1) ;i++)
                      { 

                          bool igual= sonIgualesInfo(infoCadena(set1,cadena1),infoCadena(set2,cadena2));
                          


                          
                          if(!igual){valgrin=valgrin+1000;};
 if(anterior(inicioCadena(cadena1),cadena1)!=NULL) {                         
                          set1=(anterior(finalCadena(cadena1),cadena1));
                          set2=(anterior(finalCadena(cadena2),cadena1));
                          }
if(anterior(inicioCadena(cadena1),cadena1)!=NULL) {
                         cadena1=removerDeCadena(finalCadena(cadena1),cadena1);
                         cadena2=removerDeCadena(finalCadena(cadena2),cadena2);
}


                      }

  

                
                  } else {valgrin=100;}


  liberarCadena(cadena1);
  liberarCadena(cadena2);
  return (valgrin==0);   
}

/*
  Devuelve el resultado de concatenar 'c2' después de 'c1'.
  La 'TCadena' resultado no comparte memoria ni con 'c1' ni con 'c2'.
  El tiempo de ejecución en el peor caso es O(n1 + n2), siendo 'n1' u 'n2' la
  cantidad de elementos de 'c1' y 'c2' respectivamente.
*/
TCadena concatenar(TCadena c1, TCadena c2){
 TCadena copia1= copiarSegmento(inicioCadena(c1),finalCadena(c1),c1);
 
    if (longitud (c1)==0 && longitud (c2)==0   )
   { 
  
return copia1;
  } 
     else if (longitud (c1)!=0 && longitud (c2)!=0){
  
       copia1=insertarSegmentoDespues(copiarSegmento(inicioCadena(c2),finalCadena(c2),c2),finalCadena(copia1),copia1);
  
      return copia1; 
    
      } else if((longitud (c1)!=0 && longitud (c2)==0))
        {
  
               return copia1;
               
              
        } else if ((longitud (c2)!=0 && longitud (c1)==0)) {
         

                  copia1=insertarSegmentoDespues(copiarSegmento(inicioCadena(c2),finalCadena(c2),c2),finalCadena(copia1),copia1);
  
                return copia1;
                  

                   } 
  liberarCadena(copia1);
  
  return copia1;
  } 

/*
  Se ordena 'cad' de manera creciente según los componentes naturales de sus
  elementos.
  Devuelve 'cad'
  Precondición: ! hayNatsRepetidos(cad)
  No se debe obtener ni devolver memoria de manera dinámica.
  Se debe mantener las relaciones de precedencia entre localizadores.
  Si esVaciaCadena(cad) no hace nada.
  El tiempo de ejecución en el peor caso es O(n^2), siendo 'n' la cantidad de
  elementos de 'cad'.
*/

TCadena ordenar(TCadena cad){
assert(!hayNatsRepetidos(cad));

 TInfo PIVOT= NULL;

for (nat i=15; i>0;i--){
 if( anteriorClave(i,finalCadena(cad),cad)!=NULL){

  PIVOT=copiaInfo(infoCadena(anteriorClave(i,finalCadena(cad),cad),cad));
  
   insertarAntes(PIVOT,inicioCadena(cad),cad);
   removerDeCadena(anteriorClave(i,finalCadena(cad),cad),cad);

}
else {}

}

 return cad;
}



/*
  Cambia todas las ocurrencias de 'original' por 'nuevo' en los elementos
  de 'cad'.
  Devuelve 'cad'
  No debe quedar memoria inaccesible.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos de 'cad'.
*/







/*
  Cambia todas las ocurrencias de 'original' por 'nuevo' en los elementos
  de 'cad'.
  Devuelve 'cad'
  No debe quedar memoria inaccesible.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos de 'cad'.
*/
/*
  Cambia todas las ocurrencias de 'original' por 'nuevo' en los elementos
  de 'cad'.
  Devuelve 'cad'
  No debe quedar memoria inaccesible.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos de 'cad'.
*/


/*
  Devuelve la 'TCadena' de elementos de 'cad' que cumplen
  "menor <= natInfo (elemento) <= mayor".
  El orden relativo de los elementos en la 'TCadena' resultado debe ser el mismo
  que en 'cad'.
  Precondición: estaOrdenadaPorNaturales (cad), 'menor' <= 'mayor',
  pertenece (menor, cad), pertenece (mayor, cad).
  La 'TCadena' resultado no comparte memoria con 'cad'.
  El tiempo de ejecución en el peor caso es O(n), siendo 'n' la cantidad de
  elementos de 'cad'.
*/
TCadena subCadena(nat menor, nat mayor, TCadena cad){

  assert(estaOrdenadaPorNaturales (cad)&& menor<=mayor && pertenece (menor, cad)&& pertenece (mayor, cad));
TCadena cad2= copiarSegmento(siguienteClave(menor,inicioCadena(cad),cad),anteriorClave(mayor,finalCadena(cad),cad),cad);

    return cad2;
}

/*--------------------------------------------VVVVVVV-----------------------------------------------*/
TCadena cambiarTodos(nat original, nat nuevo, TCadena cad){
 //TInfo PIVOT= NULL;
if (esVaciaCadena(cad)) {
  return cad;
  }
  else{
nat i=0;
              while (i<(longitud(cad)))
              {

              if (natInfo(infoCadena(kesimo(i+1,cad),cad))==original)
              {
              
             TInfo dato=crearInfo(nuevo,realInfo((infoCadena(kesimo(i+1,cad),cad))));
            cad=insertarAntes(dato,kesimo(i+1,cad),cad);
            cad=removerDeCadena(kesimo(i+2,cad),cad);
              //liberarInfo(dato);
          

              } 

              i=i+1;

              }   

              } 

    return cad;
}

// nueva
/*
  Devuelve una 'TCadena' con los elementos del nivel 'l' de 'b'.
  La raíz es el nivel 1.
  La 'TCadena' resultado debe estar ordenada de manera creciente según los
  componentes naturales de sus elementos.
  Precondición: l > 0.
  La 'TCadena' devuelta no comparte memoria con 'b'.
 */
TCadena nivelEnBinario(nat l, TBinario b){
  assert(l>0);
  TCadena cad=crearCadena();
  mostrarArbol2(b,1,l,cad);
  return cad;
}




void mostrarArbol2 (TBinario arbol,nat cont, nat  l,TCadena cad){
        if (arbol==NULL) {
            //SI EL ARBOL ES vacio
            return;
        } else {
            mostrarArbol2(izquierdo(arbol),cont+1, l,cad);
            if (cont==l){
                 cad= insertarAlFinal(copiaInfo(raiz(arbol)),cad);
            }           
            mostrarArbol2(derecho(arbol),cont+1,l,cad);
        }
}



// nueva
/*
  Devuelve 'true' si y solo si en 'b' hay un camino desde la raiz hasta una
  hoja que sea compatible con 'c'.
  Decimos que una cadena y un camino son compatibles si tienen la misma
  longitud y al recorrer la cadena desde el inicio hasta el final y el camino
  desde la raíz hasta la hoja los componentes naturales de los respectivos
  elementos son iguales.
  Ver ejemplos en la letra.
  Si 'b' y 'c' son vacíos devuelve 'true'.
  El tiempo de ejecución es O(log n) en promedio, siendo 'n' es la cantidad de
  elementos de 'b'.
 */
// REVISANDO TIEMPOS
 time_t tiempos()
 {
    time_t current_time;
    current_time=time(NULL);
    ctime(&current_time);
    return current_time;
} 


bool esCamino2(TCadena c, TBinario b){
 TBinario d=b; 

 
nat longitude= longitud(c);
//----------------------COMPROBACIONES PREVIAS------------------------------------------
      //LONGITUD CADENA MAS LARGA QUE ALTURA DEL ARBOL
       if( (longitude-1)>alturaBinario(b)){return false;}


      // EL ULTIMO ELEMENTO DE LA CADENA TIENE HOJAS
        nat naturalCadena2=natInfo(infoCadena(finalCadena(c),c));
        nat result=cantidadBinario(buscarSubarbol(naturalCadena2,b))-1;
        if(result>0)
        {return false;
        }
      // NO ARRANCAN POR EL MISMO NATURAL

      nat naturalCadena3=natInfo(infoCadena(inicioCadena(c),c));
      nat naturalArbol3=natInfo(raiz(b));
 if (naturalCadena3!=naturalArbol3){return false;}


//---------------------------------------------------------------------------------------


nat naturalCadena;
nat naturalArbol;
 naturalCadena=natInfo(infoCadena(inicioCadena(c),c));
 naturalArbol=natInfo(raiz(b));



  for (nat i=1;(i)<longitude;i++){

   
    d=buscarSubarbol(naturalCadena,d);
    naturalArbol=natInfo(raiz(d));
    if(naturalCadena==naturalArbol && pertenece(naturalCadena,nivelEnBinario(i,b))){
   // printf("\n tiempo en este punto %ld \n", tiempos());

    } else {

      return false;

  
    }
   // if()
      c=copiarSegmento(siguiente(inicioCadena(c),c),finalCadena(c),c);
      naturalCadena=natInfo(infoCadena(inicioCadena(c),c));
      d=buscarSubarbol(naturalCadena,d);

    //ctime devuelve 26 caracteres pero tambien se podría usar un puntero de char

    
   // printf("\n tiempo en este punto %ld \n", tiempos());




    } 
//REVISO EL ULTIMO DESGRACIADO NODO---------------------

naturalCadena=natInfo(infoCadena(finalCadena(c),c));
naturalArbol=natInfo(raiz(buscarSubarbol(naturalCadena,b)));
 if (naturalCadena!=naturalArbol){return false;}
//----------------------------

return true;
  }


























bool esCamino(TCadena c, TBinario b){
 TBinario d=b; 
  TCadena f=copiarSegmento(inicioCadena(c),finalCadena(c),c);
 
nat longitude= longitud(c);
//----------------------COMPROBACIONES PREVIAS------------------------------------------
      //LONGITUD CADENA MAS LARGA QUE ALTURA DEL ARBOL
       if( (longitude-1)>alturaBinario(b)){
          liberarCadena(f);
         return false;}


      // EL ULTIMO ELEMENTO DE LA CADENA TIENE HOJAS
        nat naturalCadena2=natInfo(infoCadena(finalCadena(c),c));
        nat result=cantidadBinario(buscarSubarbol(naturalCadena2,b))-1;
        if(result>0)
        {
          
           liberarCadena(f);
          return false;
        }
      // NO ARRANCAN POR EL MISMO NATURAL

      nat naturalCadena3=natInfo(infoCadena(inicioCadena(c),c));
      nat naturalArbol3=natInfo(raiz(b));
 if (naturalCadena3!=naturalArbol3){
   liberarCadena(f);
   return  false;}


//---------------------------------------------------------------------------------------


nat naturalCadena;
nat naturalArbol;
 naturalCadena=natInfo(infoCadena(inicioCadena(c),c));
 naturalArbol=natInfo(raiz(b));



  for (nat i=1;(i)<longitude;i++){

   
    d=buscarSubarbol(naturalCadena,d);
   
    if(buscarSubarbol(natInfo(infoCadena(siguiente(inicioCadena(f),f),f)),d)!=NULL){
   


    } else {
      liberarCadena(f);
      return false;

  
    }
   // if()
      f=removerDeCadena(inicioCadena(f),f);
      naturalCadena=natInfo(infoCadena(inicioCadena(f),f));
      d=buscarSubarbol(naturalCadena,d);

    //ctime devuelve 26 caracteres pero tambien se podría usar un puntero de char

    
   // printf("\n tiempo en este punto %ld \n", tiempos());




    } 
//REVISO EL ULTIMO DESGRACIADO NODO---------------------

naturalCadena=natInfo(infoCadena(finalCadena(c),c));
naturalArbol=natInfo(raiz(buscarSubarbol(naturalCadena,b)));
 if (naturalCadena!=naturalArbol){
   liberarCadena(f);
   return false;}
//----------------------------
 liberarCadena(f);
return true;
  }




/*
  Devuelve un 'TConjunto' con los elementos que pertenecen a 'c1' y 'c2'.
  El 'TConjunto' resultado no comparte memoria ni con 'c1' no con 'c2'.
  El tiempo de ejecucion en el peor caso es O(n1 + n2 + n.log n),
  siendo 'n1' y 'n2' la cantidad de elementos de 'c1' y 'c2' respectivamente y
  'n' la del 'TConjunto' resultado.
 */
TConjunto interseccionDeConjuntos2(TConjunto c1, TConjunto c2){
 TConjunto unionConjunto = unionDeConjuntos(c1,c2);
 TIterador unionIterador = iteradorDeConjunto(unionConjunto);
 TConjunto  compiladoConjunto = crearConjunto();
 TConjunto nuevoConjunto = crearConjunto();

 //imprimrIterador(unionIterador);
 nat n=0;
 unionIterador=reiniciarIterador(unionIterador);
 n=actualEnIterador(unionIterador);
 while(estaDefinidaActual(unionIterador)){
       n=actualEnIterador(unionIterador);
       
      if((perteneceAConjunto(n,c1)&&perteneceAConjunto(n,c2)))
        {
       nuevoConjunto=singleton(n);
       compiladoConjunto=unionDeConjuntos(compiladoConjunto,nuevoConjunto);
        }
       unionIterador=avanzarIterador(unionIterador);
    }

 return compiladoConjunto;

}


/*
  Devuelve un 'TConjunto' con los elementos que pertenecen a 'c1' y 'c2'.
  El 'TConjunto' resultado no comparte memoria ni con 'c1' no con 'c2'.
  El tiempo de ejecucion en el peor caso es O(n1 + n2 + n.log n),
  siendo 'n1' y 'n2' la cantidad de elementos de 'c1' y 'c2' respectivamente y
  'n' la del 'TConjunto' resultado.
 */
TConjunto interseccionDeConjuntos(TConjunto c1, TConjunto c2){
 //TConjunto unionConjunto = unionDeConjuntos(c1,c2);
 //TIterador unionIterador = iteradorDeConjunto(unionConjunto);
 TConjunto  compiladoConjunto = crearConjunto();
 TConjunto  nuevoConjunto = crearConjunto();

 //imprimrIterador(unionIterador);
 int n=0;

 while(n<500)
 {

      if((perteneceAConjunto(n,c1)&& perteneceAConjunto(n,c2)))
        {
       nuevoConjunto=singleton(n);
       compiladoConjunto=unionDeConjuntos(compiladoConjunto,nuevoConjunto);
        }
        n=n+1;
 }
liberarConjunto(nuevoConjunto);

 return compiladoConjunto;

}